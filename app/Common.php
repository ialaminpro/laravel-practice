<?php

namespace App;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;

/**
 * Class Common, this class is to use project common functions
 *
 * @package App
 */
class Common
{
    //const FROM_ADDRESS = 'admin@embdesignshop.com';
    //const FROM_NAME = 'Embo Design';

//Get Month From Date
   public static function getMonth($time)
    {
        try{
		$date = new Carbon( $time ); 
		return $date->month;
        }catch (\Exception $exception){
            echo $exception->getMessage();
        }
    }
//Get Year From Date
   public static function getYear($time)
    {
        try{
		$date = new Carbon( $time ); 
		return $date->year;
        }catch (\Exception $exception){
            echo $exception->getMessage();
        }
    }    


public static function convertCurrency($amount, $from, $to){
	if($amount != 0){
		if($to == 'BDT'){ return 0;}
	try{
		$data = file_get_contents("https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to");
		preg_match("/<span class=bld>(.*)<\/span>/",$data, $converted);
		$converted = preg_replace("/[^0-9.]/", "", $converted[1]);
		return number_format(round($converted, 3),2);
	}catch(\Exception $e){ return 'Unreadable'; }

	}else{ return 0; }
}
 
}//End