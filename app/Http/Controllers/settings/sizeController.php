<?php
namespace App\Http\Controllers\settings;

use App\Models\Size;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class sizeController extends Controller
{
  
    protected function create(Request $request)
    {
        for ($i = 0; $i < count($request->input('sizeName')); $i++) {
            Size::create([
            'name' => $request->input('sizeName')[$i]
            ]);
        };
        $data = array(
            'status' => 200,
            'reason' => 'Add  New Size successfully'
        );
        return response()->json($data);
    }
}
