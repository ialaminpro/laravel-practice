<?php
namespace App\Http\Controllers\settings;

use App\Models\Color;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class colorController extends Controller
{
  
    
    protected function create(Request $request)
    {
        
        $insert = Color::create([
            'display_name' => $request->input('colorName'),
            'color_code' => $request->input('colorCode')
            ]);
      
        if($insert){
            $data = array(
            'status' => 200,
            'reason' => 'Add  New Color successfully'
        );
    }else{
        $data = array(
            'status' => 0,
            'reason' => 'Add  New Color Not successfully'
        );
        }
        return response()->json($data);
    }
}
