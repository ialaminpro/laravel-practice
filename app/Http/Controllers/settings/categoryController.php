<?php
namespace App\Http\Controllers\settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;
use File;



class categoryController extends Controller
{
  
    protected function create(Request $request)
    {
        // dd($request->input());
        $destinationPath = public_path('/uploads/category');
        $path  ='';  
        if($request->file('cat_image')){
        $image =$request->file('cat_image');
		$input = time().'.'.$image->getClientOriginalExtension();			
        $image->move($destinationPath, $input);	
        $path = '/uploads/category/'.$input;
        }

                
        $insert = Category::create([
            'cat_name' => $request->input('catName'),
            'parent_id' => $request->input('parent'),
            'image_file_name' => $path 
            ]);
      
        if($insert){
            $data = array(
            'status' => 200,
            'reason' => 'Add  New Category successfully'
        );
    }else{
        $data = array(
            'status' => 0,
            'reason' => 'Add  New Category Not successfully'
        );
        }
        return response()->json($data);
    }
}
