<?php
namespace App\Http\Controllers\settings;

use App\Models\Dimesion;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class dimensionController extends Controller
{
  
    protected function create(Request $request)
    {
        for ($i = 0; $i < count($request->input('dimensionName')); $i++) {
            Dimesion::create([
            'name' => $request->input('dimensionName')[$i]
            ]);
        };
        $data = array(
            'status' => 200,
            'reason' => 'Add  New Dimension successfully'
        );
        return response()->json($data);
    }
}
