<?php
namespace App\Http\Controllers\settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WeightUnit;


class weightController extends Controller
{
  
    protected function create(Request $request)
    {
        for ($i = 0; $i < count($request->input('weightName')); $i++) {
            WeightUnit::create([
            'name' => $request->input('weightName')[$i]
            ]);
        };
        $data = array(
            'status' => 200,
            'reason' => 'Add  New Weight successfully'
        );
        return response()->json($data);
    }
}
