<?php
namespace App\Http\Controllers\settings;

use App\Models\Tag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class tagController extends Controller
{
  
    protected function create(Request $request)
    {
        for ($i = 0; $i < count($request->input('tagName')); $i++) {
           Tag::create([
            'name' => $request->input('tagName')[$i]
            ]);
        };
        $data = array(
            'status' => 200,
            'reason' => 'Add  New Tag successfully'
        );
        return response()->json($data);
    }
}
