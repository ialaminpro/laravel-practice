<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
* Admin controller to manage admin activitiess
*
* @return \Illuminate\Http\Response
*/

class AdminController extends Controller{

	/**
	* Admin Dashboard Landing Function
	*/
	public static function dashboard(){
		$data['page'] = '';
		return view('backend.landing.dashboard',$data);
	} 



} //End Admin Class  
