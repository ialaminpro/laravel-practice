<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Requests\StoreProduct;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Validator;
use File;
use DB;
use App\Common;
use App\Models\Product;
use App\Models\Size;
use App\Models\ProductDetails;
use App\Models\Color;
use App\Models\WeightUnit;
use App\Models\Dimesion;
use App\Models\Tag;
use App\Models\ProductTags;
use App\Models\Category;
use App\Models\ProductCategory;
use App\Models\ProductImages;
use App\Models\ProductStock;


/**
* Admin Product management System function
*
* @return \Illuminate\Http\Response
*/ 

class productController extends Controller
{

	public $data;
	public function __construct()
	{
		$this->data['size'] = Size::all();
		$this->data['color'] = Color::all();
		$this->data['weight_unit'] = WeightUnit::all();
		$this->data['dimesions'] = Dimesion::all();
		$this->data['tags'] = Tag::all();
		$this->data['categories'] = Category::all();
		DB::enableQueryLog(); 

	}
	


   public function allProduct(Product $prod){
		   
	$prod = Product::with('status')->orderBy('product_id','desc')->get();
	// dd(DB::getQueryLog());
	// dump(DB::getQueryLog());
	// dump($prod);
	$product_details = array();
	foreach($prod->toArray() as $pro){
	
		$collection = collect($pro);
		$categories = Product::find($pro['product_id'])->categories()->select('cat_name')->get();
		$categories = collect(array_pluck($categories, 'cat_name'))->implode(',');
		$detailsPro = Product::find($pro['product_id'])->detailsPro()->with('sizes')->with('colors')->get();
		$detailsPro = $collection->merge(['detailsPro' => $detailsPro->toArray()]);
		$product_details[] = $detailsPro->merge(['categories' => $categories]);
	}
	// dump($product_details);

	$product = json_decode(json_encode((object) $product_details), FALSE);


	$allcategory = $this->data['categories'];
			
	 return view('backend.product.list',compact('product','allcategory'));
   }

	/**
	* Admin Product add page
	*/
   public function addProduct(Product $product){
    
	return view('backend.product.create',['attributes' => $this->data]);

   }

   public function store(Request $request)
    {
	
		// dd($request->input());
		DB::beginTransaction();
		try{	

        // if(Auth::check()){
			// DB::enableQueryLog(); 
		
					$destinationPath = public_path('/uploads/product_cpecifications');
					$file = $request->file('specFile');
					$specification_file_path = '';
					if($file){
					$input = time().'.'.$file->getClientOriginalExtension();			
					$file->move($destinationPath, $input);
					$specification_file_path = '/uploads/product_cpecifications/'.$input;
					}
					
            $product = Product::create([
                'title' => $request->input('proTitle'),
                'description' => $request->input('proDesc'),
                'specification' => $request->input('proSpec'),
                'specification_file_path' => $specification_file_path,
				'return_policy' => $request->input('returnPolicy'),
				'replace_description' => $request->input('replace_description'),
				'purchase_description' => $request->input('purchase_description'),   
				'warranty_description' => $request->input('warentyDesc'),
                'product_status_id' => 1
			]);
			// dump($product);

			// dd($product);
			foreach($request->input('category') as $category){
				// echo $category;
				$product_category = DB::table('product_categories')->insert([ 
					'product_id' => $product->product_id,
					'cat_id' => $category
				]);

				// dump($product_category);
			}
			
			
			foreach($request->input('proTag') as $tag){
			
				$product_tags = DB::table('product_tags')->insert([ 
					'product_id' => $product->product_id,
					'tag_id' => $tag[0]
				]);

				// dump($product_tags);
			}

			

			foreach($request->input('proSku') as $index => $specific_info){
				//echo $specific_info;
				$product_details = ProductDetails::create([
					'product_id' => $product->product_id,
					'weight_unit_id' => $request->input('weight_id')[$index],
					'weight' => $request->input('weight')[$index],
					'size_id' => $request->input('proSize')[$index],
					'color_id' => $request->input('proColor')[$index],
					'dimension' => $request->input('proDim')[$index],
					'is_default' => $request->input('is_default')[$index],
					'product_sku' => $request->input('proSku')[$index]
				]);
				// dump($product_details);
				$destinationPath = public_path('/uploads/products');
				$img_id = 0;	
				foreach($request->file('proImg')[$index] as $image){
					
					$input = $product->product_id.'-'.$product_details->product_detail_id.'-'.$img_id.'.'.$image->getClientOriginalExtension();			
					$image->move($destinationPath, $input);
					// echo $request->input('is_default_img')[0][$index];
					$product_images =  DB::table('product_images')->insert([
					
							'image_path' => '/uploads/products/'.$input, 			
							'is_default' => $request->input('is_default_img')[0][$index], 
							'image_type' => 'Original', 	
							'product_detail_id' => $product_details->product_detail_id
						]);
						// dump($product_images);
					$img_id++;
				}
				
				$product_stocks = DB::table('product_stocks')->insert([ 
					'quantity' => $request->input('qty')[$index], 
					'stock_in_date' => date("Y-m-d"),
					'product_detail_id' => $product_details->product_detail_id
				]);
				// dump($product_stocks);
				// dd(DB::getQueryLog());

				$product_prices =  DB::table('product_prices')->insert([
					
					'purchase_price' => $request->input('purchase_price')[$index], 
					'sales_price' => $request->input('sales_price')[$index], 
					'product_detail_id' => $product_details->product_detail_id
				]);
				// dump($product_prices);
				// dump($request->input());
				// dump(DB::getQueryLog());
				
			}

			DB::commit();
		
		
			$data = array(
				'status' => 200,
				'reason' => 'Add  New Product successfully'
			);
			return response()->json($data);
		
		} catch(\Exception $e){
		//if there is an error/exception in the above code before commit, it'll rollback
		DB::rollBack();
		$data = array(
			'status' => 0,
			'reason' => $e->getMessage()
		);
		return response()->json($data);
		}

	
	}
	
	
	/**
	* Admin Product add page
	*/ 
   public function productEdit($product_id){
		
		DB::enableQueryLog(); 
		$this->data['product']  = $product = Product::find($product_id);
	
		try {
			$this->data['detailsPro'] =  $detailsPro = $product->detailsPro()->with('stock')->with('price')->with('colors')->with('sizes')->with('images')->get();
		} catch (\Exception $e) {
			// throw $this->createNotFoundException('The author does not exist');
			return $e->getMessage();
		}
	
		// $detailsPro = json_decode(json_encode((object) $detailsPro), FALSE);
		// dd($detailsPro);
		// dd(DB::getQueryLog());
		$category = ProductCategory::join('categories','categories.cat_id','=','product_categories.cat_id')
		->select('categories.cat_id')
		->where('product_id', '=', $product_id)->get();

		$selected_category = array();
		foreach($category as $cat){
				$selected_category[] = $cat->cat_id;  
		}
		$this->data['selected_category'] = $selected_category;  

		$tag = ProductTags::join('tags','tags.id','=','product_tags.tag_id')
		->select('product_tags.tag_id')
		->where('product_id', '=', $product_id)->get();

		$selected_tag = array();
		foreach($tag as $value){
				$selected_tag[] = $value->tag_id;  
		}
		$this->data['selected_tag'] = $selected_tag; 

	return view('backend.product.edit', $this->data);	
	
   }

   public function update_status(Request $request)
   {
	//  DB::enableQueryLog(); 
	 $Update = Product::where('product_id', $request->input('product_id'))
							   ->update([
									   'product_status_id'=> $request->input('status'),
									   
							   ]);			   
	if($Update){
		$data = array(
			'status' => 200,
			'reason' => 'Product updated successfully'
		);
		return response()->json($data);
	}		
	else {
		$data = array(
			'status' => 0,
			'reason' => 'Product could not be updated'
		);
		return response()->json($data);
	}
	
   }

   public function update(Request $request)
   {

	// dd($request->input());
	//  DB::enableQueryLog(); 

		 DB::beginTransaction();
		try{
		$product_id = $request->input('product_id');
		$destinationPath = public_path('/uploads/product_cpecifications');
					$file = $request->file('specFile');
					$specification_file_path = '';
					if($file){
					$input = time().'.'.$file->getClientOriginalExtension();			
					$file->move($destinationPath, $input);
					$specification_file_path = '/uploads/product_cpecifications/'.$input;
		}

		 $Update = Product::where('product_id', $request->input('product_id'))
							   ->update([
								'title' => $request->input('proTitle'),
								'description' => $request->input('proDesc'),
								'specification' => $request->input('proSpec'),								
								'return_policy' => $request->input('returnPolicy'),
								'replace_description' => $request->input('replace_description'),
								'purchase_description' => $request->input('purchase_description'),   
								'warranty_description' => $request->input('warentyDesc') 
							   ]);	

							   if($specification_file_path){
								$Update = Product::where('product_id', $request->input('product_id'))
								->update([
									'specification_file_path' => $specification_file_path,
								]);
							   }
							//    if(count($request->input('category'))){
							   	DB::table('product_categories')->where('product_id', '=', $product_id)->delete();

								foreach($request->input('category') as $category){
									// echo $category;
										$product_category = DB::table('product_categories')->insert([
											'product_id' => $product_id,
											'cat_id' => $category
										]);
									}
								// }
						// if(count($request->input('proTag'))){
							ProductTags::where('product_id', '=', $product_id)->delete();
							foreach($request->input('proTag') as $tag){
							
								$product_tag = DB::table('product_tags')->insert([
									'product_id' => $product_id,
									'tag_id' => $tag[0]
								]);
							// }
						}
			
							$in = 0;
							if(count($request->input('product_detail_id'))){
								foreach($request->input('product_detail_id') as $in => $product_detail_id){
									$ProductDetails = ProductDetails::where('product_detail_id', $product_detail_id)
								->update([
										'product_id' => $product_id,
										'weight_unit_id' => $request->input('weight_id')[$in],
										'weight' => $request->input('weight')[$in],
										'size_id' => $request->input('proSize')[$in],
										'color_id' => $request->input('proColor')[$in],
										'dimension' => $request->input('proDim')[$in],
										'is_default' => $request->input('is_default')[$in],
										'product_sku' => $request->input('proSku')[$in]   
								]);

								// $destinationPath = public_path('/uploads/products');
								// $img_id = 0;	
								// foreach($request->file('proImg')[$in] as $image){
									
								// 	$input = $product_id.'-'.$product_detail_id.'-'.$img_id.'.'.$image->getClientOriginalExtension();			
								// 	$image->move($destinationPath, $input);
			
								// 		$product_images = DB::table('product_images')->where('product_detail_id', $product_detail_id)
								// 		->update([
								// 			'image_path' => '/uploads/products/'.$input, 			
								// 			'is_default' => $request->input('is_default_img')[$in][0], 
								// 			'image_type' => 'Original', 	
								// 			'product_detail_id' => $product_detail_id
								// 		]);

								// 	$img_id++;
								// }

									//if($ProductDetails){
									$product_stocks = DB::table('product_stocks')->where('product_detail_id', $product_detail_id)
									->update([
										'quantity' => $request->input('qty')[$in], 
										'stock_in_date' => date("Y-m-d"),  
									]);
										if(!$product_stocks) $msg[] = "Product Stocks Update Faild";
									
									
									$product_prices = DB::table('product_prices')->where('product_detail_id', $product_detail_id)
									->update([
										'purchase_price' => $request->input('purchase_price')[$in], 
										'sales_price' => $request->input('sales_price')[$in],  
									]);

									if(!$product_prices) $msg[] = "Product Prices Update Faild";
									
									// }else{
									// 	$msg[] = 'Product Details Update Failed';
									// }
								
								}
							}
						
							$index = $in+1;

							while(count($request->input('proSku'))>$index){						
								
								$product_details = ProductDetails::create([
									'product_id' => $product_id,
									'weight_unit_id' => $request->input('weight_id')[$index],
									'weight' => $request->input('weight')[$index],
									'size_id' => $request->input('proSize')[$index],
									'color_id' => $request->input('proColor')[$index],
									'dimension' => $request->input('proDim')[$index],
									'is_default' => $request->input('is_default')[$index],
									'product_sku' => $request->input('proSku')[$index]
								]);

								$destinationPath = public_path('/uploads/products');
								$img_id = 0;	
								foreach($request->file('proImg')[$index] as $image){
									
									$input = $product_id.'-'.$product_details->product_detail_id.'-'.$img_id.'.'.$image->getClientOriginalExtension();			
									$image->move($destinationPath, $input);
									
									$product_images =  DB::table('product_images')->insert([
										
											'image_path' => '/uploads/products/'.$input, 			
											'is_default' => $request->input('is_default_img')[$index][0], 
											'image_type' => 'Original', 	
											'product_detail_id' => $product_details->product_detail_id
										]);
				
									$img_id++;
								}

								// if($product_details){
								$product_stocks =  DB::table('product_stocks')->insert([
									
									'quantity' => $request->input('qty')[$index], 
									'stock_in_date' => date("Y-m-d"),
									'product_detail_id' => $product_details->product_detail_id
								]);
								if(!$product_stocks) $msg[] = "Product Stocks Insert Faild";
								$product_prices =  DB::table('product_prices')->insert([
									
									'purchase_price' => $request->input('purchase_price')[$index], 
									'sales_price' => $request->input('sales_price')[$index], 
									'product_detail_id' => $product_details->product_detail_id
								]);
								if(!$product_prices) $msg[] = "Product Price Update Faild";
								
							// }else{
							// 	$msg[] = 'Product Details Insert Failed';
							// }

								$index++;
							}


		DB::commit();
		$data = array(
			'status' => 200,
			'reason' => 'Product Updated successfully'
		);
		return response()->json($data);
	
		} catch(\Exception $e){
		//if there is an error/exception in the above code before commit, it'll rollback
		DB::rollBack();
		// return $e->getMessage();
		$data = array(
			'status' => 0,
			'reason' => $e->getMessage()
		);
		return response()->json($data);
		}
	
   }
    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {

		dd($product);

        //

       // $company = Company::where('id', $company->id )->first();
        DB::enableQueryLog();
            $product = Product::find($product->product_id);
            //dump($company->projects);  
		   //dump(DB::getQueryLog());
        return view('backend.product.list', ['product'=>$product,'size'=>$size]);
	}
	
	

   

     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
		
		$findProduct = Product::find($request->input('product_id'));
		if(!empty($findProduct) && $findProduct->delete()){
            
            $data = array(
				'status' => 200,
				'reason' => 'Product deleted successfully'
			);
			return response()->json($data);
        }else{

		$data = array(
			'status' => 0,
			'reason' => 'Product could not be deleted'
		);
		return response()->json($data);
	}
    }

}// End of add product
