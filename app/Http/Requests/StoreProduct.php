<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {        
        return [
            'proTitle' => 'required',//|unique:posts|max:255',
            'proDesc' => 'required',
            'category' => 'required',
            'proTag' => 'required',
            'proSku' => 'required',
            'weight_id' => 'required',
            'weight' => 'required',
            'proSize' => 'required',
            'proColor' => 'required',
            'proDim' => 'required',
            'qty' => 'required',
            'purchase_price' => 'required',
            'sales_price' => 'required',
        ];
        
    }

    public function messages()
    {
        return [
            'proTitle.required' => 'The Product Title is required',
            'proDesc.required'  => 'The Product Descriptiong is required',
            'proTag.required'  => 'Fields in SKU section are required',
          
        ];
    }
}
