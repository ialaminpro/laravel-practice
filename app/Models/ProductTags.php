<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductTags extends Model
{
    protected $table = 'product_tags';
    protected $primaryKey = 'product_tag_id';

    protected $fillable = [
        'product_tag_id',
        'product_id',
        'tag_id',
    ];

}
