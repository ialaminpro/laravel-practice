<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImages extends Model
{
    protected $table = 'product_images';
    protected $primaryKey = 'product_image_id';

    protected $fillable = [
        'product_image_id',
        'product_detail_id',
        'title',
        'description',
        'alttext',
        'image_path',
        'image_type',
        'is_default',
    ];

    // public function product(){
    //     // return $this->hasMany(ProductCat::class, 'product_id');
	// 	return $this->belongsTo('App\Models\Product','product_id');
	// 	// return $this->belongsTo('App\Company');
    // }
    public function product_details(){
        return $this->hasOne(ProductDetails::class, 'product_detail_id');
		// return $this->belongsTo('App\Models\Product','product_id');
		// return $this->belongsTo('App\Company');
    }
}
