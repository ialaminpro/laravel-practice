<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'product_id';

    protected $fillable = [
        'product_id',
        'title',
        'description',
        'specification',
        'specification_file_path',
        'return_policy',
        'warranty_description',
        'purchase_description',
        'replace_description',
        'warranty_from',
        'warranty_to',
        'created_by',
        'product_status_id',
        'is_downloadable',
        'total_rating_count',
        'total_review_count',
        'average_rating_point',

    ];
   
    #one to many cat relation
    public function categories() {
        return $this->belongsToMany(Category::class,'product_categories', 'product_id', 'cat_id');
        // return $this->hasMany(ProductCategory::class,'product_id');
    }

    #one to many detail tabe relation
    public function detailsPro()
    {
      return $this->hasMany(ProductDetails::class,'product_id');
    }

   

    public function sizes() {
        // return $this->hasMany(Size::class,'id','id');
        return $this->hasManyThrough(Size::class , ProductDetails::class,'size_id','id');
    }

    
  
    
    // public function ProductCategory() {
    //     return $this->hasManyThrough(Product::class, ProductCategory::class,'product_id','product_id','product_id','product_id');
    // }
    public function ProductCategory()
    {
        // return $this->belongsTo('App\Director');
        return $this->belongsTo(ProductCategory::class,'product_id');
    }

    #one to one cat relation
   	public function price()
    {
        return $this->hasOne(ProductPrice::class,'product_detail_id','product_detail_id');
    }
    #one to one cat relation
    public function stock()
    {
        return $this->hasOne(ProductStock::class,'product_detail_id','product_detail_id');
    }
    public function colors() {
        // return $this->hasMany(Size::class,'id','id');
        // return $this->hasOne(Color::class,'color_id','id');
        // return $this->hasManyThrough(Color::class , ProductDetails::class);
    }
    public function status()
    {
        return $this->hasOne(ProductStatuses::class,'id','product_status_id');
    }
    

    // public function sizes()
    // {
    //     return $this->belongsToMany(Size::class);
    // }


}
