<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dimesion extends Model
{
    protected $table = 'dimesions';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'is_active',
    ];
}
