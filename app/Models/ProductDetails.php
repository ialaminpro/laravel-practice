<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDetails extends Model
{
    protected $table = 'product_details';
    protected $primaryKey = 'product_detail_id';

    protected $fillable = [
        'product_categories_id',
        'product_id',
        'weight_unit_id',
        'weight',
        'dimension',
        'color_id',
        'size_id',
        'product_sku',
        'current_stock_availble',
        'current_number_of_stock_processing',
        'total_rating_count',
        'total_review_count',
        'average_rating_point',
        'is_default',
        'created_at',
        'updated_at',
    ];

    public function product(){
        // return $this->hasMany(ProductCat::class, 'product_id');
		return $this->belongsTo('App\Models\Product','product_id');
		// return $this->belongsTo('App\Company');
    }

    public function product_images(){
        return $this->hasMany(ProductImages::class, 'product_detail_id');
    }


    
    public function sizes() {
        return $this->hasMany(Size::class,'id','size_id');
    }

    public function colors() {
        return $this->hasMany(Color::class,'id','color_id');
        // return $this->belongsTo(Color::class,'id','color_id');
    }
    public function price() {
        return $this->belongsTo(ProductPrice::class,'product_detail_id','product_detail_id');
    }
    public function stock() {
        return $this->belongsTo(ProductStock::class,'product_detail_id','product_detail_id');
    }
    public function images() {
        return $this->hasMany(ProductImages::class,'product_detail_id','product_detail_id');
    }
    
}

