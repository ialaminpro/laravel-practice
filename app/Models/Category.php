<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'cat_id';

    protected $fillable = [
        'cat_id',
        'cat_name',
        'cat_uri',
        'parent_id',
        'image_file_name',
        'status',
        'created_at',
        'updated_at',
    ];



    public function ProductCategory() {
        return $this->hasMany(ProductCategory::class,'cat_id');
    }
    
    public function product() {
        return $this->belongsToMany(Product::class);
    }
}
