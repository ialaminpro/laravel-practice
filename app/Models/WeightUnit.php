<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WeightUnit extends Model
{
    protected $table = 'weight_units';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'is_active',
    ];


    // public function productCategories() {
    //     return $this->hasMany('App\Models\ProductCategory');
    // }
    
    // public function product() {
    //     return $this->hasManyThrough('App\Models\Product', 'App\Models\Category');
    // }
}
