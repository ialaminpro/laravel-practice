<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCat extends Model
{
    protected $table = 'product_categories';
    protected $primaryKey = 'product_categories_id';

    protected $fillable = [
        'product_categories_id',
        'product_id',
        'cat_id',
    ];

    public function product(){
        // return $this->hasMany(ProductCat::class, 'product_id');
		return $this->belongsTo('App\Models\Product','product_id');
		// return $this->belongsTo('App\Company');
    }

  
}
