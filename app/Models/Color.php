<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $table = 'colors';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'display_name',
        'color_code',
        'is_active',
    ];
    public function ProductDetails() {
        return $this->belongsTo('App\Models\ProductDetails','color_id','id');
    }
    public function product() {
        return $this->belongsTo('App\Models\Product','color_id','id');
    }
    // public function productCategories() {
    //     return $this->hasMany('App\Models\ProductCategory');
    // }
    
    // public function product() {
    //     return $this->hasManyThrough('App\Models\Product', 'App\Models\Category');
    // }
}
