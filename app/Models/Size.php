<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $table = 'sizes';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'is_active',
    ];

    public function ProductDetails() {
        return $this->hasMany('App\Models\ProductDetails','size_id','id');
    }
    // public function productCategories() {
    //     return $this->hasMany('App\Models\ProductCategory');
    // }
    
    // public function product() {
    //     return $this->hasManyThrough('App\Models\Product', 'App\Models\Category');
    // }
}
