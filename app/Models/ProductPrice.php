<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $table = 'product_prices';
    protected $primaryKey = 'product_prices_id';

    protected $fillable = [
        'product_prices_id',
        'product_detail_id',
        'purchase_price',
        'sales_price',
        'effected_date_from',
        'effected_date_to'
    ];
    public function product() {
        return $this->belongsTo('App\Models\Product','product_detail_id','product_detail_id');
    }

    // public function productCategories() {
    //     return $this->hasMany('App\Models\ProductCategory');
    // }
    
    // public function product() {
    //     return $this->hasManyThrough('App\Models\Product', 'App\Models\Category');
    // }
}
