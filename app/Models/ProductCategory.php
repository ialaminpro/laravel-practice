<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categories';
    protected $primaryKey = 'product_categories_id';

    protected $fillable = [
        'product_categories_id',
        'product_id',
        'cat_id',
    ];


    public function product()
    {
        return $this->belongsTo(Product::class,'product_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'cat_id');
    }

}
