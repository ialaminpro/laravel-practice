<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStock extends Model
{
    protected $table = 'product_stocks';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'product_detail_id',
        'quantity',
        'stock_in_date',
    ];
    public function product() {
        return $this->belongsTo('App\Models\Product','product_detail_id','product_detail_id');
    }

    // public function productCategories() {
    //     return $this->hasMany('App\Models\ProductCategory');
    // }
    
    // public function product() {
    //     return $this->hasManyThrough('App\Models\Product', 'App\Models\Category');
    // }
}
