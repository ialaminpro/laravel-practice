<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStatuses extends Model
{
    protected $table = 'product_statuses';
    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'name',
        'is_active',
      
    ];
    public function product() {
        return $this->belongsTo('App\Models\Product','id');
    }

    // public function productCategories() {
    //     return $this->hasMany('App\Models\ProductCategory');
    // }
    
    // public function product() {
    //     return $this->hasManyThrough('App\Models\Product', 'App\Models\Category');
    // }
}
