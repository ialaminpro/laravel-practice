-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 23, 2018 at 04:09 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mqnpkowp_product_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(10) NOT NULL,
  `cat_name` varchar(192) COLLATE utf8mb4_bin NOT NULL,
  `cat_uri` varchar(128) COLLATE utf8mb4_bin NOT NULL,
  `parent_id` int(10) NOT NULL DEFAULT '0',
  `image_file_name` varchar(128) COLLATE utf8mb4_bin DEFAULT NULL,
  `status` enum('Active','Inactive','Deleted') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_uri`, `parent_id`, `image_file_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Category - 01', '', 1, '', 'Active', '2018-05-23 05:45:41', '2018-05-23 05:45:41'),
(2, 'Category - 02', '', 1, '', 'Active', '2018-05-23 05:46:00', '2018-05-23 05:46:00'),
(3, 'Category - 03', '', 0, '', 'Active', '2018-05-23 05:46:13', '2018-05-23 05:46:13'),
(4, 'Category - 04', '', 0, '', 'Active', '2018-05-23 05:47:04', '2018-05-23 05:47:04');

-- --------------------------------------------------------

--
-- Table structure for table `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `display_name` varchar(128) DEFAULT NULL,
  `color_code` varchar(32) DEFAULT NULL,
  `is_active` enum('Active','Inactive') DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `colors`
--

INSERT INTO `colors` (`id`, `display_name`, `color_code`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Red', '#eb0e0e', 'Active', '2018-05-23 05:49:28', '2018-05-23 05:49:28'),
(2, 'Black', '#000000', 'Active', '2018-05-23 05:49:40', '2018-05-23 05:49:40'),
(3, 'Green', '#16f053', 'Active', '2018-05-23 05:50:06', '2018-05-23 05:50:06');

-- --------------------------------------------------------

--
-- Table structure for table `dimesions`
--

CREATE TABLE `dimesions` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `is_active` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dimesions`
--

INSERT INTO `dimesions` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '12x21', 'Active', '2018-05-23 05:50:52', '2018-05-23 05:50:52'),
(2, '31x13', 'Active', '2018-05-23 05:50:52', '2018-05-23 05:50:52'),
(3, '32x23', 'Active', '2018-05-23 05:50:52', '2018-05-23 05:50:52');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(10) NOT NULL,
  `title` varchar(192) COLLATE utf8mb4_bin NOT NULL,
  `description` text COLLATE utf8mb4_bin,
  `specification` text COLLATE utf8mb4_bin,
  `specification_file_path` varchar(64) COLLATE utf8mb4_bin DEFAULT NULL,
  `return_policy` text COLLATE utf8mb4_bin,
  `warranty_description` text COLLATE utf8mb4_bin,
  `purchase_description` text COLLATE utf8mb4_bin,
  `replace_description` text COLLATE utf8mb4_bin,
  `warranty_from` date DEFAULT NULL,
  `warranty_to` date DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `product_status_id` tinyint(4) NOT NULL,
  `is_downloadable` tinyint(1) DEFAULT '1',
  `total_rating_count` decimal(10,2) DEFAULT NULL,
  `total_review_count` int(11) DEFAULT NULL,
  `average_rating_point` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `title`, `description`, `specification`, `specification_file_path`, `return_policy`, `warranty_description`, `purchase_description`, `replace_description`, `warranty_from`, `warranty_to`, `created_by`, `product_status_id`, `is_downloadable`, `total_rating_count`, `total_review_count`, `average_rating_point`, `created_at`, `updated_at`) VALUES
(1, 'Samsung', 'This is a Samsung Mobile.', '<p><span style=\"color: rgb(51, 51, 51);\">Product Specification</span><br></p>', '/uploads/product_cpecifications/1527078340.png', '<p><span style=\"color: rgb(51, 51, 51);\">Product Return Policy</span><br></p>', '<p><span style=\"color: rgb(51, 51, 51);\">Warranty Description</span><br></p>', '<p><span style=\"color: rgb(51, 51, 51);\">Product Purchase and Delivery</span><br></p>', '<p><span style=\"color: rgb(51, 51, 51);\">Product Replace and Warranty</span><br></p>', NULL, NULL, 0, 2, 1, NULL, NULL, NULL, '2018-05-23 06:25:40', '2018-05-23 08:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `product_categories_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`product_categories_id`, `product_id`, `cat_id`) VALUES
(3, 1, 1),
(4, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE `product_details` (
  `product_detail_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `weight_unit_id` int(11) DEFAULT NULL,
  `weight` varchar(16) DEFAULT NULL,
  `dimension` varchar(16) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  `size_id` int(11) DEFAULT NULL,
  `product_sku` varchar(128) DEFAULT NULL,
  `current_stock_availble` int(11) NOT NULL COMMENT 'Always update-able ',
  `current_number_of_stock_processing` int(11) NOT NULL DEFAULT '0' COMMENT 'current_number_of_stock_processing but not delivered',
  `total_rating_count` decimal(10,2) DEFAULT NULL,
  `total_review_count` int(11) DEFAULT NULL,
  `average_rating_point` decimal(10,2) DEFAULT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`product_detail_id`, `product_id`, `weight_unit_id`, `weight`, `dimension`, `color_id`, `size_id`, `product_sku`, `current_stock_availble`, `current_number_of_stock_processing`, `total_rating_count`, `total_review_count`, `average_rating_point`, `is_default`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '2', '1', 2, 1, 'SKU-S-1', 0, 0, NULL, NULL, NULL, 1, '2018-05-23 06:25:40', '2018-05-23 08:02:03'),
(2, 1, 1, '1', '2', 3, 2, 'SKU-S-2', 0, 0, NULL, NULL, NULL, 1, '2018-05-23 06:25:41', '2018-05-23 08:02:03'),
(3, 1, 1, '3', '1', 1, 1, 'SKU-S-3', 0, 0, NULL, NULL, NULL, 0, '2018-05-23 08:02:03', '2018-05-23 08:02:03');

-- --------------------------------------------------------

--
-- Table structure for table `product_files`
--

CREATE TABLE `product_files` (
  `id` int(11) NOT NULL COMMENT 'Downloadable Product',
  `product_detail_id` int(11) NOT NULL,
  `file_path` varchar(256) NOT NULL COMMENT 'serverpath.filename.gui_id.extension',
  `original_file_name` varchar(128) NOT NULL COMMENT 'View Original Name'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_offers`
--

CREATE TABLE `product_has_offers` (
  `product_has_offer_id` int(11) NOT NULL,
  `offer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `minimum_buying_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `product_image_id` int(10) NOT NULL,
  `product_detail_id` int(10) NOT NULL,
  `title` varchar(192) COLLATE utf8mb4_bin DEFAULT NULL,
  `description` text COLLATE utf8mb4_bin,
  `alttext` varchar(192) COLLATE utf8mb4_bin DEFAULT NULL,
  `image_path` varchar(256) COLLATE utf8mb4_bin DEFAULT NULL,
  `image_type` enum('Original','Thumb') COLLATE utf8mb4_bin NOT NULL DEFAULT 'Original',
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`product_image_id`, `product_detail_id`, `title`, `description`, `alttext`, `image_path`, `image_type`, `is_default`) VALUES
(1, 1, NULL, NULL, NULL, '/uploads/products/1-1-0.jpg', 'Original', 1),
(2, 1, NULL, NULL, NULL, '/uploads/products/1-1-1.jpg', 'Original', 1),
(3, 2, NULL, NULL, NULL, '/uploads/products/1-2-0.jpg', 'Original', 0),
(4, 3, NULL, NULL, NULL, '/uploads/products/1-3-0.jpg', 'Original', 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_prices`
--

CREATE TABLE `product_prices` (
  `product_prices_id` int(11) NOT NULL,
  `product_detail_id` int(11) NOT NULL,
  `purchase_price` decimal(10,2) NOT NULL,
  `sales_price` decimal(10,2) NOT NULL,
  `effected_date_from` date DEFAULT NULL,
  `effected_date_to` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_prices`
--

INSERT INTO `product_prices` (`product_prices_id`, `product_detail_id`, `purchase_price`, `sales_price`, `effected_date_from`, `effected_date_to`, `created_at`, `updated_at`) VALUES
(1, 1, '120001.00', '125001.00', NULL, NULL, '2018-05-23 12:25:41', NULL),
(2, 2, '100001.00', '105001.00', NULL, NULL, '2018-05-23 12:25:41', NULL),
(3, 3, '122222.00', '122333.00', NULL, NULL, '2018-05-23 14:02:03', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_statuses`
--

CREATE TABLE `product_statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_statuses`
--

INSERT INTO `product_statuses` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Active', 1, NULL, '2018-05-23 12:36:02'),
(2, 'Inactive', 0, NULL, '2018-05-23 12:35:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_stocks`
--

CREATE TABLE `product_stocks` (
  `id` int(11) NOT NULL,
  `product_detail_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `stock_in_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stocks`
--

INSERT INTO `product_stocks` (`id`, `product_detail_id`, `quantity`, `stock_in_date`) VALUES
(1, 1, 0, '2018-05-23'),
(2, 2, 0, '2018-05-23'),
(3, 3, 12, '2018-05-23');

-- --------------------------------------------------------

--
-- Table structure for table `product_tags`
--

CREATE TABLE `product_tags` (
  `product_tag_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_tags`
--

INSERT INTO `product_tags` (`product_tag_id`, `product_id`, `tag_id`) VALUES
(3, 1, 1),
(4, 1, 1),
(5, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `sizes`
--

CREATE TABLE `sizes` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `is_active` enum('Active','Inactive') NOT NULL DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sizes`
--

INSERT INTO `sizes` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, '2', 'Active', '2018-05-23 05:49:06', '2018-05-23 05:49:06'),
(2, '3', 'Active', '2018-05-23 05:49:06', '2018-05-23 05:49:06'),
(3, '4', 'Active', '2018-05-23 05:49:06', '2018-05-23 05:49:06');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `is_active` enum('Active','Inactive') DEFAULT 'Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Tag - 01', 'Active', '2018-05-23 05:52:01', '2018-05-23 05:52:01'),
(2, 'Tag - 02', 'Active', '2018-05-23 05:52:01', '2018-05-23 05:52:01'),
(3, 'Tag - 03', 'Active', '2018-05-23 05:52:01', '2018-05-23 05:52:01');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `weight_units`
--

CREATE TABLE `weight_units` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `is_active` tinyint(4) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weight_units`
--

INSERT INTO `weight_units` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'KG', 0, '2018-05-23 05:47:48', '2018-05-23 05:47:48'),
(2, 'Liter', 0, '2018-05-23 05:47:48', '2018-05-23 05:47:48'),
(3, 'G', 0, '2018-05-23 05:47:48', '2018-05-23 05:47:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `colors`
--
ALTER TABLE `colors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dimesions`
--
ALTER TABLE `dimesions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`product_categories_id`);

--
-- Indexes for table `product_details`
--
ALTER TABLE `product_details`
  ADD PRIMARY KEY (`product_detail_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`product_image_id`);

--
-- Indexes for table `product_prices`
--
ALTER TABLE `product_prices`
  ADD PRIMARY KEY (`product_prices_id`);

--
-- Indexes for table `product_statuses`
--
ALTER TABLE `product_statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_tags`
--
ALTER TABLE `product_tags`
  ADD PRIMARY KEY (`product_tag_id`);

--
-- Indexes for table `sizes`
--
ALTER TABLE `sizes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `weight_units`
--
ALTER TABLE `weight_units`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `dimesions`
--
ALTER TABLE `dimesions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `product_categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_details`
--
ALTER TABLE `product_details`
  MODIFY `product_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `product_image_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `product_prices`
--
ALTER TABLE `product_prices`
  MODIFY `product_prices_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_statuses`
--
ALTER TABLE `product_statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_stocks`
--
ALTER TABLE `product_stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_tags`
--
ALTER TABLE `product_tags`
  MODIFY `product_tag_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sizes`
--
ALTER TABLE `sizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `weight_units`
--
ALTER TABLE `weight_units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
