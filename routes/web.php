<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------
| Backend Settings Section Routs
|--------------------------------------------------------------------------
*/


Route::get('/','backend\AdminController@dashboard');
Route::get('/admin','backend\AdminController@dashboard');

//Product Management Section
// Route::resource('product', 'backend\productController');
Route::get('/product/all','backend\productController@allProduct');
Route::get('/product/add','backend\productController@addProduct');

Route::get('/product/edit/{id}','backend\productController@productEdit');
Route::get('/product/{id}/edit','backend\productController@edit');
Route::post('product/update_status', 'backend\productController@update_status');
Route::post('product/update', 'backend\productController@update')->name('product.update');
Route::post('product/delete', 'backend\productController@destroy');
// Route::resource('product', 'backend\productController');
Route::post('product/store', 'backend\productController@store')->name('product.store');

Route::post('/settings/tag/create','settings\tagController@create');
Route::post('/settings/dimension/create','settings\dimensionController@create');
Route::post('/settings/color/create','settings\colorController@create');
Route::post('/settings/size/create','settings\sizeController@create');
Route::post('/settings/weight/create','settings\weightController@create');
Route::post('/settings/category/create','settings\categoryController@create');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
