
$(".rateYo").rateYo({
  starWidth: "15px",
  readOnly: true
});

$(window).on("load",function(){
  $(".scroll_content").mCustomScrollbar();
});


$( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 500,
    values: [ 100, 300 ],
    slide: function( event, ui ) {
    $( "#amount" ).val( "৳ " + ui.values[ 0 ] + " - ৳ " + ui.values[ 1 ] );
  }
});
$( "#amount" ).val( "৳ " + $( "#slider-range" ).slider( "values", 0 ) +
  " - ৳ " + $( "#slider-range" ).slider( "values", 1 ) );

//Grid list view
$('.active_grid').click(function(){
    $('#product_list').removeClass('list_cotent');
    $('.filter_icons>a').removeClass('active');
    $(this).addClass('active');
});
$('.active_list').click(function(){
    $('#product_list').addClass('list_cotent');
    $('.filter_icons>a').removeClass('active');
    $(this).addClass('active');
});

//custom
$('.color_list>li').click(function(){
    $(this).parent('.color_list').children('li').removeClass('active');
    $(this).addClass('active');
});

$('.size_list>li').click(function(){
    $(this).parent('.size_list').children('li').removeClass('active');
    $(this).addClass('active');
});

// Flex slider product details
$('#detailsCarousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 140,
    itemMargin: 5,
    asNavFor: '#detailsSlider'
});

$('#detailsSlider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#detailsCarousel"
});

// The slider being synced must be initialized first
function initFlexModal() {
    $('#carousel').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      itemWidth: 120,
      itemMargin: 5,
      asNavFor: '#slider'
    });
};
function initFlex01Modal() {
    $('#slider').flexslider({
      animation: "slide",
      controlNav: false,
      animationLoop: false,
      slideshow: false,
      sync: "#carousel"
    });
};

//initFlexModal();
//initFlex01Modal();

$('#QuickeViewModal').on('shown.bs.modal', function () {
    initFlexModal();
    initFlex01Modal();
});

//Image zoom plugin
$(".zoom").elevateZoom({
    zoomWindowFadeIn: 500,
    zoomWindowFadeOut: 500,
    lensFadeIn: 500,
    lensFadeOut: 500,
    zoomType: "window",
    lensSize: 100,
    zoomWindowWidth:540,
    zoomWindowHeight:450,
});

//Image zoom plugin
$(".details_zoom").elevateZoom({
    zoomWindowFadeIn: 500,
    zoomWindowFadeOut: 500,
    lensFadeIn: 500,
    lensFadeOut: 500,
    zoomType: "window",
    lensSize: 100,
    zoomWindowWidth:416,
    zoomWindowHeight:400,
});

//Related product slider
$('#related-product').flexslider({
    animation: "slide",
    animationLoop: true,
    itemWidth: 180,
    itemMargin: 18,
    minItems: 5,
    maxItems: 6
});

//Add product qtn
$('.btn-number').click(function(e){
    e.preventDefault();
    
    fieldName = $(this).attr('data-field');
    type      = $(this).attr('data-type');
    var input = $("input[name='"+fieldName+"']");
    var currentVal = parseInt(input.val());
    if (!isNaN(currentVal)) {
        if(type == 'minus') {
            
            if(currentVal > input.attr('min')) {
                input.val(currentVal - 1).change();
            } 
            if(parseInt(input.val()) == input.attr('min')) {
                $(this).attr('disabled', true);
            }

        } else if(type == 'plus') {

            if(currentVal < input.attr('max')) {
                input.val(currentVal + 1).change();
            }
            if(parseInt(input.val()) == input.attr('max')) {
                $(this).attr('disabled', true);
            }

        }
    } else {
        input.val(0);
    }
});
$('.input-number').focusin(function(){
   $(this).data('oldValue', $(this).val());
});
$('.input-number').change(function() {
    
    minValue =  parseInt($(this).attr('min'));
    maxValue =  parseInt($(this).attr('max'));
    valueCurrent = parseInt($(this).val());
    
    name = $(this).attr('name');
    if(valueCurrent >= minValue) {
        $(".btn-number[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        alert('Sorry, the minimum value was reached');
        $(this).val($(this).data('oldValue'));
    }
    if(valueCurrent <= maxValue) {
        $(".btn-number[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
    } else {
        //alert('Sorry, the maximum value was reached');
        //$(this).val($(this).data('oldValue'));
    }
});


//Checkout form steps
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches

$(".next").click(function(){
    if(animating) return false;
    animating = true;
    
    current_fs = $(this).parent();
    next_fs = $(this).parent().next();
    
    //activate next step on progressbar using the index of next_fs
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    
    //show the next fieldset
    next_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale current_fs down to 80%
            scale = 1 - (1 - now) * 0.2;
            //2. bring next_fs from the right(50%)
            left = (now * 50)+"%";
            //3. increase opacity of next_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({
        'transform': 'scale('+scale+')',
        'position': 'absolute'
      });
            next_fs.css({'left': left, 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
            current_fs.css({
                'position': 'relative'
            });
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".previous").click(function(){
    if(animating) return false;
    animating = true;
    
    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();
    
    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    
    //show the previous fieldset
    previous_fs.show(); 
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({'left': left});
            previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
        }, 
        duration: 800, 
        complete: function(){
            current_fs.hide();
            animating = false;
        }, 
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

//address Checkout
$('#address-check').change(function(){
    if($(this).is(':checked')){
        $('#billing-address').addClass('hidden');
    }
    else{
        $('#billing-address').removeClass('hidden');
    }
});

//review rating
$(".customerRateYo").rateYo({
    starWidth: "20px",
});

$(".rateYo_comment").rateYo({
    starWidth: "15px",
    readOnly: true
});

//POp over 
$("[data-toggle=popover]").popover({
    html : true,
    content: function() {
      var content = $(this).attr("data-popover-content");
      return $(content).children(".popover-body").html();
    },
    title: function() {
      var title = $(this).attr("data-popover-content");
      return $(title).children(".popover-heading").html();
    }
});

$('.forget_pass').click(function(){
    $('#loginModal').modal('hide');
    $('#forgetModal').modal('show');
});

$('.back_to_login').click(function(){
    $('#loginModal').modal('show');
    $('#forgetModal').modal('hide');
});

$('#xs-nav').click(function(){
    $('#nav_collapse').toggleClass('collapse');
});