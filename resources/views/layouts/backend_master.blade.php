<!DOCTYPE html>
<html lang="en">

<!-- Head section include Here -->
 @include('layouts.includes.head')

 <!-- Custom page css add here -->
  @yield('css')

<body>

    <div id="wrapper">

<!-- Navigation -->
 @include('layouts.includes.navigation')

    <!-- alert message START -->
        <div class="modal fade alert" role="dialog" id="alert-modal" style="z-index: 99999">
            <div class="modal-dialog" style="width: 350px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <div id="alert-error-msg">
                            <div>
                                <i class="material-icons">error_outline</i>
                            </div>
                            <p class="text-danger"></p>
                        </div>

                        <div id="alert-success-msg">
                            <div>
                                <i class="material-icons">check</i>
                            </div>
                            <p class="text-success"></p>
                        </div>

                        <button class="btn btn-primary" data-dismiss="modal" id="alert-ok">ok</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- alert message End -->

        <!-- alert message START -->
        <div class="modal fade alert" role="dialog" id="warning-modal" style="z-index: 99999">
            <div class="modal-dialog" style="width: 350px">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <div id="warning-error-msg">
                            <div>
                                <i class="material-icons">error_outline</i>
                            </div>
                            <p class="text-danger"></p>
                        </div>
                        <input type="hidden" id="remove_id" val=""/>

                        <button class="btn btn-primary" id="warning-ok">Ok</button>
                        <button class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </div>

            </div>
        </div>
        <!-- alert message End -->

<!-- Page Main Content -->
@yield('content')


    </div>
    <!-- /#wrapper -->

<!-- Footer Source Content -->
 @include('layouts.includes.footer')

 <!-- Custom page JS add here -->
  @yield('js')

</body>

</html>
