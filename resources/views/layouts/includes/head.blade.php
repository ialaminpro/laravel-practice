<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Product Crud</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ url('back_assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ url('back_assets/css/plugins/morris.css') }}" rel="stylesheet">
    
    <!-- Bootstrap Core CSS -->
    <link href="{{ url('back_assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="{{ url('back_assets/plugins/Nestable-master/jquery.nestable.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ url('back_assets/css/template.css') }}" rel="stylesheet">
    <link href="{{ url('back_assets/css/responsive.css') }}" rel="stylesheet">

</head>