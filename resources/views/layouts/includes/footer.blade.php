    <!-- jQuery -->
    <script src="{{ url('back_assets/js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ url('back_assets/js/bootstrap.min.js') }}"></script>
    
    <!-- page level plugins -->
    <script src="{{ url('back_assets/plugins/tinymce/tinymce.min.js') }}"></script>">
    <script src="{{ url('back_assets/plugins/Nestable-master/jquery.nestable.js') }}"></script>

    <!-- include summernote css/js -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <!--script type="text/javascript" src="plugins/datatables/datatables.min.js"></script-->

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

    <script src="{{ url('back_assets/custom/sweetalert.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>



    <!-- custom plugins-->
    <script src="{{ url('back_assets/js/custom.js')}}"></script>



    <script>
        function show_success_message($message){
            $('#alert-modal').modal('show');
            $('#alert-error-msg').hide();
            $('#alert-success-msg').show();
            $('#alert-success-msg p').html($message);
        }
        function show_error_message(message){
            $('#alert-modal').modal('show');
            $('#alert-error-msg').show();
            $('#alert-success-msg').hide();
            $('#alert-error-msg p').html(message);
        }
        function show_warning_message(id,message){
            $('#remove_id').val(id);
            $('#warning-error-msg').show();
            $('#warning-error-msg p').html(message);
            $('#warning-modal').modal('show');
        }
    </script>