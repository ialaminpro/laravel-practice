@extends('layouts.backend_master')

@section('css')
    <link href="{{ url('/back_assets/custom/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div id="page-wrapper" class="col-md-4">
        <div class="main-body-content">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="Page-heading">
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- Response Message Section From controller -->
                            <div class="alert alert-success" id="success_message" style="display:none"></div>
                            <div class="alert alert-danger" id="error_message" style="display: none"></div>

                            <h1 class="page-header">
                                Product List
                            </h1>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Page content 1 START-->
                <div class="Page-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">

                                </div>

                                <div class="panel-body">

                                    <div class="col-sm-12">

                                        <select class="form-control two-val-sort01" id="product-list-cat">
                                            <option value="">Filter by Category</option>
                                            <option value="Category 01">Category 01</option>
                                            <option value="Category 02">Category 02</option>
                                        </select>

                                        <select class="form-control two-val-sort02" id="product-list-status">
                                            <option value="">Filter by Status</option>
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>

                                        <div class="table-responsive margin-top-40">
                                            <table id="product-list-table" class="table table-striped table-hover table-bordered datatable">
                                                <thead>
                                                <tr>
                                                    <th width="50px" class="text-center">SL.</th>
                                                    <th>Product Name</th>
                                                    <th>Categories</th>
                                                    <th>Size</th>
                                                    <th>Color</th>
                                                    <th>Stock</th>
                                                    <th>Status</th>
                                                    <th class="text-center">Active/Inactive</th>
                                                    <th class="text-center">Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="category_list">
                                               
                                                
                                                 @foreach($product as $index => $value)

                                                  
                                                <tr >
                                                    <td class="text-center">{{ ++$index }}</td>
                                                            <td class="catTd">{{ $value->title }}</td>
                                                            <td>{{ $value->cat_name }}</td>
                                                            <td>{{ $value->size }}</td>
                                                            <td>{{ $value->display_name }}</td>
                                                            <td>{{ $value->quantity }}</td>
                                                            <td>{{ $value->status }}</td>
                                                
                                                            <td >
                                                                <label class="switch toggle-switch">
                                                                <input data-id ='{{$value->product_id}}' class="status_check" type="checkbox" @if($value->status=="Active") checked @endif >
                                                                    <span class="slider round"></span>
                                                                </label>
                                                            </td>
    
                                                            <td class="text-center">
                                                                    <span class="action-icon ">
                                                                          
                                                                        <a class="btn btn-primary editProduct" href="/product/edit/{{ $value->product_id }}" title="Edit">
                                                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                        </a>
                                                                    </span>
    
    
                                                                <span class="action-icon deleteBtn">
                                                                        <a data-id ='{{$value->product_id}}' class="btn btn-danger delete_button" id="" href="javascript:;" title="Delete">
                                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                                        </a>
                                                                    </span>
                                                            </td>
                                                        </tr>
                                                    @endforeach  

                                                   
                                                    <tr>
                                                        <td class="text-center">02</td>
                                                        <td class="catTd">Product two</td>
                                                        <td>Category 02</td>
                                                        <td>XL</td>
                                                        <td>Red</td>
                                                        <td>15</td>
                                                        <td>Inactive</td>
                                                        <td>
                                                            <label class="switch toggle-switch">
                                                                <input class="status_check" type="checkbox">
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </td>

                                                        <td class="text-center">
                                                                <span class="action-icon editBtn">
                                                                    <a class="btn btn-primary editProduct" href="#" title="Edit">
                                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                                    </a>
                                                                </span>


                                                                <span class="action-icon deleteBtn">
                                                                        <a data-id ='2332' class="btn btn-danger delete_button" id="" href="javascript:;" title="Details">
                                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                                        </a>
                                                                    </span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>


                <!-- /.container-fluid -->
            </div>

        </div>
        <!-- /#page-wrapper -->
    </div>


    <!-- alert message START -->
    <div class="modal fade alert" role="dialog" id="warning_modal" style="z-index: 99999">
        <div class="modal-dialog" style="width: 350px">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div id="alert-error-msg">
                        <div>
                            <i class="material-icons">error_outline</i>
                        </div>
                        <p class="text-danger"></p>
                    </div>

                    <div id="alert-success-msg">
                        Are you sure you want to delete this?
                    </div>

                    <input type="hidden" name="product_id" id="product_id">

                    <button class="btn btn-danger" id="delete_button">Delete</button>
                    <button class="btn btn-primary" data-dismiss="modal" id="alert-ok">Cancel</button>
                </div>
            </div>

        </div>
    </div>
    <!-- alert message End -->




@endsection


@section('js')
    <script src="{{ url('/back_assets/custom/sweetalert.js')}}"></script>

    <script type="text/javascript">
        $(document).ready(function(){
            var doctor_list = $('#product-list-table').DataTable();
            $('#product-list-cat').change(function(){
                var select_val = $(this).val();
                doctor_list
                    .columns(2)
                    .search(select_val)
                    .draw();
            });

            $('#product-list-status').change(function(){
                var select_val = $(this).val();
                doctor_list
                    .columns(6)
                    .search(select_val)
                    .draw();
            });
        });

        function submit_form(){
            var title = $('#proTitle').val();
            var description = $('#proDesc').val();
            var specification = $('#proSpec').val();
            var status = $('#proStatus').val();
            var nature = $('#proNature').val();
            var validate = '';

            if(title==''){
                validate = validate+"Title is required</br>";
            }
            if(description==''){
                validate = validate+"Description is required";
            }
            if(specification==''){
                validate = validate+"Specification is required";
            }
            if(status==''){
                validate = validate+"Status is required";
            }
            if(nature==''){
                validate = validate+"Nature is required";
            }

            if(validate==''){
                var formData = new FormData($('#product_form')[0]);
                var url = '{{ url('/product/store') }}';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    async: false,
                    success: function (data) {
                        if(data.status == 200){
                            $('#success_message').show();
                            $('#error_message').hide();
                            $('#success_message').html(data.reason);
                            //window.location.href="{{ url('admin') }}";
                        }
                        else{
                            $('#success_message').hide();
                            $('#error_message').show();
                            $('#error_message').html(data.reason);
                        }
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }
            else{
                $('#success_message').hide();
                $('#error_message').show();
                $('#error_message').html(validate);
            }
        }

        //tag edit modal function
        $(document).on('click','.editProduct',function(){
            var text = $(this).parents('tr').children('.catTd').text();
            var id = $(this).attr('cus-data');
            $('#edit_modal').modal('show');
            $('#tagEname').val(text);
            $('#catId').val(id);
        })

        function delete_product(product_id){
            $('#product_id').val(product_id);
            $('#warning_modal').modal('show');
        }

        $(document).on('click','.delete_button',function(){
           // alert( $(this).attr('data-id'));
            var product_id = $(this).attr('data-id');
           // var product_id = $('#product_id').val();
            var url = '{{ url('/product/delete') }}';
            $.ajax({
                type: "POST",
                url: url,
                data: {product_id:product_id,'_token':'{{ csrf_token() }}'},
                async: false,
                success: function (data) {
                    if(data.status == 200){
                        show_success_message(data.reason);
                        location.reload();
                    }
                    else{
                        show_error_message(data.reason);
                    }
                }
            });
        })

        $(document).on('click','.status_check',function(){
            

            if($(this).is(':checked')) {
                var status = 1;
            }else{
                var status = 2;
            }
          
            var product_id = $(this).attr('data-id');
            var url = '{{ url('/product/update_status') }}';
            $.ajax({
                type: "POST",
                url: url,
                data: {product_id:product_id,status:status,'_token':'{{ csrf_token() }}'},
                async: false,
                success: function (data) {
                    
                    if(data.status == 200){
                        show_success_message(data.reason);
                        location.reload();
                    }
                    else{
                        show_error_message(data.reason);
                    }
                    
                }
            });
        })

        function tagSave(){
            var tagName = $('#tagName').val();
            if(tagName = ''){
                alert(tagName);
                return false;
            }
        }

        // Tag delete By ajax From Here-------------
        function tagDel(id){
            swal({
                        title: "Are you sure?",
                        text: "Your will not be able to recover this imaginary file!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        confirmButtonText: "Yes, delete it!",
                        closeOnConfirm: false
                    },
                    function(){
                        var dataString = {
                            _token: "{{ csrf_token() }}",
                            id:id
                        };
                        $.ajax({
                            type: "POST",
                            url: "{{ url('/settings/category/delete') }}",
                            data: dataString,
                            dataType: "JSON",
                            cache : false,
                            beforeSend: function() {

                            },
                            success: function(data){ console.log(data);
                                if(data.status == 200){
                                    $('#cat'+a).remove();
                                    swal("Deleted!", "Your data has been deleted.", "success");
                                }
                                if(data.status == 501){
                                    swal("Error!", "Delete Error.", "Error");
                                }
                            } ,error: function(xhr, status, error) {
                                alert(error);
                            },
                        });


                    });
        }


    </script>

@endsection
