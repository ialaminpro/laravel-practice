@extends('layouts.backend_master')

@section('css')
    <link href="{{ url('/back_assets/custom/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('content')
    <div id="page-wrapper" class="col-md-4">
        <div class="main-body-content">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="Page-heading">
                    <div class="row">
                        <div class="col-lg-12">

                            <!-- Response Message Section From controller -->
                            <div class="alert alert-success" id="success_message" style="display:none"></div>
                            <div class="alert alert-danger" id="error_message" style="display: none"></div>

                            <h1 class="page-header">
                                Product <small>Product Create</small>
                            </h1>
                        </div>
                    </div>
                </div>
                <!-- /.row -->

                <!-- Page content 1 START-->
                <div class="Page-body">

                   
                    <div class="row">
                        <?php
                        // print_r($product->title);
                    // foreach ($size as $key => $value) {
                    //    echo '<pre>';
                    //     print_r($value->name);
                    //     echo '<pre>';
                    // }
                            // dd($size);
                            ?>

                        <div class="col-md-12">
                                <form id="product_form" method="post" action="{{ route('product.update') }}" enctype="multipart/form-data">
                            {{-- <form id="product_form" action="{{ url('/product/update_product') }}" method="POST" enctype="multipart/form-data"> --}}
                                {{ csrf_field() }}
                                <input type="hidden" name="product_id" id="product_id" value="{{ $product->product_id }}" />
                                <input type="hidden" name="status" id="product_status" value="{{ $product->product_status_id }}" /> 
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        General Info:
                                    </div>



                                    <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="margin-bottom-15">
                                                                <label>Product Title <span class="mandatory"> *</span></label>
                                                                <input type="text" class="form-control" value="{{ $product->title }}" name="proTitle" id="proTitle">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-12 col-sm-12">
                                                            <div class="w100 input-group margin-bottom-15">
                                                                <label>Product Category<span class="mandatory"> *</span></label>
                                                                <?php //dd($selected_category) ?> 
                                                                <select class="form-control selectpicker" multiple name="category[]" id="category">
                                                                        @foreach($categories as $category)
                                                                        <option value="{{$category->cat_id}}"
                                                                        {{-- @endforeach --}}
                                                                        <?php
                                                                        if(is_array($selected_category)){
                                                                            if(in_array($category->cat_id, $selected_category)){ echo "selected"; }
                                                                        }
                                                                         ?>
                                                                 >{{ $category->cat_name }}</option>
                                                             @endforeach
                                                         </select>
                                                         <p class="sm-text">
                                                             <a href="javascript:;" data-toggle="modal" data-target="#add_cat_modal">Click to add category</a>
                                                         </p>
                                                     </div>
                                                 </div>
    
    
                                                                        
    
    
                                                       {{-- <div class="col-lg-2">
                                                            <div class="margin-bottom-15 cat_image_preview_wrapper">
                                                                <img id="cat_image_preview" src="http://uploads.webflow.com/img/placeholder-thumb.svg">
                                                            </div>
                                                       </div>
    
    
                                                      <div class="col-lg-4 col-sm-12">
                                                            <div class="margin-bottom-15">
                                                                <label>Feature Image</label>
                                                                <input type="file" class="form-control" onchange="loadFile(event)" value="" name="" id="">
                                                            </div>
                                                        </div>  --}}
    
                                                        <div class="col-lg-6 col-sm-12">
                                                                <div class="form-check margin-bottom-15">
                                                                    <label>
                                                                       <input type="checkbox" name="is_downloadable" value="1" @if($product->is_downloadable==1) checked @endif> <span class="label-text">Downloadable</span> 
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <div class="input-group ">
                                                            <label>Product Description<span class="mandatory"> *</span></label>
                                                            <textarea class="form-control summernote" rows="4"  name="proDesc" id="proDesc">{{ $product->description }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
    
    
                                    <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <div class="row">
                                                    <div class="col-sm-6"><p>Specific Info:</p></div>
                                                    <div class="col-sm-6">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-lg-offset-4">
                                                                <select class="form-control" id="add_extra_type">
                                                                    <option value="">Select Type</option>
                                                                    <option value="Weight">Weight</option>
                                                                    <option value="Size">Size</option>
                                                                    <option value="Color">Color</option>
                                                                    <option value="Dimension">Dimension</option>
                                                                    <option value="Tag">Tag</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <button type="button" id="add_extra_type_btn" class="btn btn-primary">Add</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
    
                                            
    
    
                                        
                                            <div class="panel-body" style="padding: 5px;">
                                                <?php
                                                //    dd($detailsPro );
                                                    $count=0;?>
                                                    @if(count($detailsPro))
                                                @foreach($detailsPro as $dpro)
                                                <input type="hidden" name="product_detail_id[]" id="product_detail_id" value="{{ $dpro->product_detail_id }}" /> 
                                                <div id="product-info-panel">
                                                    <div class="panel panel-default">
                                                        <div class="panel-body" style="padding: 5px;">
                                                            <div class="row no-margin">
                                                                <div class="product_create_group col">
                                                                    <div class="input-group margin-bottom-15">
                                                                        <label> SKU<span class="mandatory"> *</span></label>
                                                                        <input type="text" class="form-control" value="{{ $dpro->product_sku }}" name="proSku[]" id="proSku">
                                                                    </div>
                                                                </div>
        
                                                                <div class="product_create_group col">
                                                                    <div class="margin-bottom-15">
                                                                        <label>Weight Unit<span class="mandatory"> *</span></label>
                                                                        <select class="form-control" name="weight_id[]" id="proSku">
                                                                            @foreach($weight_unit as $we)
                                                                                <option value="{{ $we->id }}"
                                                                                        @if($dpro->weight_unit_id  == $we->id)
                                                                                        selected
                                                                                        @endif
                                                                                >{{ $we->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
    
                                                                        
                                                                   
    
                                                            <div class="product_create_group col">
                                                                <div class="input-group margin-bottom-15">
                                                                    <label>Weight<span class="mandatory"> *</span></label>
                                                                    <input type="text" class="form-control" value="{{ $dpro->weight }}" name="weight[]" id="proSku">
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="input-group margin-bottom-15">
                                                                    <label>Size<span class="mandatory"> *</span></label>
                                                                    <select class="form-control" name="proSize[]" id="proSize">
                                                                        @foreach($size as $sizes)
                                                                            <option value="{{ $sizes->id }}"
                                                                                    @if($dpro->size_id  == $sizes->id)
                                                                                    selected
                                                                                    @endif
                                                                            >{{ $sizes->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="margin-bottom-15 p-color">
                                                                    <label>Color <span class="mandatory"> *</span></label>
                                                                    <select class="form-control color_select selectpicker" name="proColor[]" id="proColor">
                                                                        @foreach($color as $colors)
                                                                            <option value="{{ $colors->id }}" style="background: #5cb85c; color: #fff;"
                                                                                    @if($dpro->color_id  == $colors->id)
                                                                                    selected
                                                                                    @endif
                                                                            >{{ $colors->display_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="margin-bottom-15">
                                                                    <label>Dimention<span class="mandatory"> *</span></label>
                                                                    <select class="form-control" name="proDim[]" id="proDim">
                                                                        @foreach($dimesions as $dim)
                                                                            <option value="{{ $dim->id }}"
                                                                                    @if($dpro->dimension  == $dim->id)
                                                                                    selected
                                                                                    @endif
                                                                                >{{ $dim->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>

                                                            <div class="product_create_group col">
                                                                <div class="margin-bottom-15 tag-select p-tag">
                                                                    <label class="pull-left">Tag<span class="mandatory"> *</span></label>
                                                                    <select class="form-control selectpicker" name="proTag[{{ $count }}][]" id="proTag" multiple >
                                                                        @foreach($tags as $tag)
                                                                            <option value="{{ $tag->id }}" @if(in_array($tag->id, $selected_tag)) selected @endif>{{ $tag->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
    
                                                            <div class="clearfix"></div>
    
                                                            <div class="product_create_group col">
                                                                <div class="input-group ">
                                                                    <label>Quantity<span class="mandatory"> *</span></label>
                                                                    <input type="text" class="form-control" value="{{ $dpro->current_stock_availble }}" name="qty[]" id="qty">
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="input-group ">
                                                                    <label>Purchase Price<span class="mandatory"> *</span></label>
                                                                   <?php
                                                                   //dd($dpro->price->purchase_price);
                                                                   ?>
                                                                    <input type="text" class="form-control" value="{{ $dpro->price->purchase_price }}" name="purchase_price[]" id="">
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="input-group ">
                                                                    <label>Salse Price<span class="mandatory"> *</span></label>
                                                                    <input type="text" class="form-control" value="{{ $dpro->price->sales_price }}" name="sales_price[]" id="">
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="">
                                                                    <label>Product Image<span class="mandatory"> *</span></label><br>
                                                                    <!-- <input type="file" class="form-control" value="" name="proImg[0][]" id="proImg" multiple> -->
                                                                    <button type="button" class="btn btn-info p-img-btn btn-block" data-toggle="modal" data-target="#imgModal_{{ $count+1 }}">Upload</button>
                                                                </div>
    
                                                                <!-- Image Uploads -->
                                                                <div class="modal fade" id="imgModal_{{ $count+1 }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                                                    <div class="modal-dialog modal-lg" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                                <h4 class="modal-title" id="myModalLabel">Add Product Images</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <?php
                                                                                    
                                                                                    // dd($dpro)
                                                                                    
                                                                                    ?>
                                                                                @foreach($dpro->images as $image)
                                                                                    <div class="row">
                                                                                        <div class="col-lg-1 col-lg-offset-1 col-sm-2 no-padding">
                                                                                            <div class="margin-bottom-15 p_image_preview_wrapper">
                                                                                                <img class="p_image_preview" src="{{ url('/').$image->image_path }}">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-5 col-sm-4">
                                                                                            <div class="form-group margin-bottom-0 margin-top-25">
                                                                                                <input type="hidden" name="product_old_img[{{ $count }}][]" value="{{ $image->image_path }}" class="form-control p-img-file">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-2 col-sm-3">
                                                                                            <div class="form-check margin-top-25 def_img_check">
                                                                                                <label>
                                                                                                    <input type="radio" name="default_img_{{ $count }}[{{ $count }}][]" @if($image->is_default==1) checked @endif > <span class="label-text">Default Image</span>
                                                                                                    <input type="hidden" name="is_default_img[{{ $count }}][]" value="1">
                                                                                                </label>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-lg-2 col-sm-3">
                                                                                            <button type="button" class="btn pull-right btn-primary add_product_img" data-id="0">
                                                                                        <span>
                                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                        </span>
                                                                                            </button>
    
                                                                                            <button type="button" disabled class="btn pull-right btn-danger dlt_product_img">
                                                                                        <span>
                                                                                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                                                        </span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                @endforeach
   
                                                                                <div class="row">
                                                                                    <div class="col-lg-1 col-lg-offset-1 col-sm-2 no-padding">
                                                                                        <div class="margin-bottom-15 p_image_preview_wrapper">
                                                                                            <img class="p_image_preview" src="http://via.placeholder.com/1200x900">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-5 col-sm-4">
                                                                                        <div class="form-group margin-bottom-0 margin-top-25">
                                                                                            <input type="file" name="proImg[{{ $count }}][]" class="form-control p-img-file">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-2 col-sm-3">
                                                                                        <div class="form-check margin-top-25 def_img_check">
                                                                                            <label>
                                                                                                <input type="radio" name="default_img_{{ $count }}[{{ $count }}][]" > <span class="label-text">Default Image</span>
                                                                                                <input type="hidden" name="is_default_img_{{ $count }}[{{ $count }}][]" value="1">
                                                                                            </label>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-lg-2 col-sm-3">
                                                                                        <button type="button" class="btn pull-right btn-primary add_product_img" data-id="0">
                                                                                        <span>
                                                                                            <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                                        </span>
                                                                                        </button>
    
                                                                                        <button type="button" disabled class="btn pull-right btn-danger dlt_product_img">
                                                                                        <span>
                                                                                            <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                                                        </span>
                                                                                        </button>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->
                                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
    
                                                            <div class="product_create_group col">
                                                                <div class="form-check margin-top-22 text-center p-def product_default">
                                                                    <label>
                                                                        <input type="radio" name="default_product[]" @if($dpro->is_default==1) checked @endif > <span class="label-text">Default</span>
                                                                        <input type="hidden" name="is_default[]" value="1"/>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <?php $count++; ?>
                                                            
                                                            <div class="product_create_group col margin-top-22 pull-right">
                                                                <button type="button" class="btn pull-right btn-primary add_product_btn">
                                                                    <span>
                                                                        <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                                                    </span>
                                                                </button>
    
                                                                <button type="button" disabled class="btn pull-right btn-danger dlt_product_btn">
                                                                    <span>
                                                                        <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
   
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            Descriptive Info:
                                        </div>
    
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="margin-bottom-15">
                                                        <label>Product Specification file</label>
                                                        <input type="file" class="form-control" value="" name="specFile" id="specFile">
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-12">
                                                    <div class="input-group w100 margin-bottom-15">
                                                        <label>Product Specification</label>
                                                        <textarea class="form-control summernote" rows="6" name="proSpec" id="proSpec">{{ $product->specification }}</textarea>
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-12">
                                                    <div class="input-group w100 margin-bottom-15">
                                                        <label>Product Return Policy</label>
                                                        <textarea class="form-control summernote" rows="6" name="returnPolicy" id="returnPolicy">{{ $product->return_policy }}</textarea>
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-12">
                                                    <div class="input-group w100 margin-bottom-15">
                                                        <label>Product Replace and Warranty</label>
                                                        <textarea class="form-control summernote" rows="6" name="replace_description" id="replace_description">{{ $product->replace_description }}</textarea>
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-12">
                                                    <div class="input-group w100 margin-bottom-15">
                                                        <label>Product Purchase and Delivery</label>
                                                        <textarea class="form-control summernote" rows="6" name="purchase_description" id="purchase_description">{{ $product->purchase_description }}</textarea>
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-12">
                                                    <div class="w100 input-group margin-bottom-15">
                                                        <label>Warranty Description</label>
                                                        <textarea class="form-control summernote" rows="6" value="" name="warentyDesc" id="proWarentyDesc">{{ $product->warranty_description }}</textarea>
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-6">
                                                    <div class="form-check margin-bottom-15">
                                                        <label>
                                                            <input type="checkbox" name="check"> <span class="label-text">Automatic Share</span>
                                                        </label>
                                                    </div>
                                                </div>
    
                                                <div class="col-lg-6">
                                                    <div class="">
                                                        <button type="button" class="btn btn-primary pull-right  margin-left-10" id="save_button">Publish</button>
                                                        <button type="button" class="btn btn-danger pull-right  margin-left-10" id="">Cancel</button>
                                                        @if($product->status_id !=3)
                                                        <button type="button" class="btn btn-info pull-right  margin-left-10" id="save_draft_button">Save as Draft</button>
                                                        @endif
                                                    </div>
                                                </div>
    
    
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
    
    
                    <!-- /.container-fluid -->
                </div>
    
            </div>
            <!-- /#page-wrapper -->
        </div>
    
        <!-- Modal -->
        <!-- Add Category modal -->
        <div id="add_cat_modal" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <form id="category_update_form" action="{{ url('/settings/category/create') }}" method="post">
                            {{ csrf_field() }}
                            <div class="modal-header">
                                <h4 class="no-margin">
                                    Add Category
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </h4>
                            </div>
                         
                            <div class="modal-body">
                                <div class="row">
                                    <div class="alert alert-success login-success" id="edit_success" style="display:none"></div>
                                    <div class="alert alert-danger login-failed" id="edit_error" style="display:none"></div>
                                    <div class="col-sm-6">
                                        <div class="">
                                            <div class="post-footer">
                                                <div class="article-post-area" style="">
                                                    <label>Category Name</label>
                                                    <input type="text" class="form-control"  name="catName" id="" value="">
                                                    <div class="article-post-send">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" class="form-control" value="" name="id" id="">   
                                    </div>
        
                                    <div class="col-lg-6">
                                        <div class="margin-bottom-15">
                                            <label>Parent Category</label>
                                            <select class="form-control" name="parent" id="catEparent">
                                                <option value="0">Parent</option>
                                                <option value="1">Parent 01</option>
                                                <option value="0=2">Parent 02</option>
                                            </select>
                                        </div>
                                    </div>
        
                                    <div class="col-lg-10">
                                        <div class="margin-bottom-15">
                                            <label>Category Image</label>
                                            <input type="file" class="form-control" value="" name="cat_image" id="" onchange="add_cat_loadFile(event)">
                                        </div>
                                    </div>
        
                                    <div class="col-lg-2">
                                        <div class="margin-bottom-15 cat_image_preview_wrapper">
                                            <img id="add_cat_image_preview" src="http://uploads.webflow.com/img/placeholder-thumb.svg">
                                        </div>
                                    </div>
        
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="button" id="update_button" class="btn btn-primary">Submit</button>
                                
                            </div>
                        </form>
                    </div>
        
                </div>
            </div>
    
        <!-- Image Uploads -->
        <div class="modal fade" id="imgModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Product Images</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-1 col-lg-offset-1 col-sm-2 no-padding">
                                <div class="margin-bottom-15 p_image_preview_wrapper">
                                    <img class="p_image_preview" src="http://via.placeholder.com/1200x900">
                                </div>
                            </div>
                            <div class="col-lg-5 col-sm-4">
                                <div class="form-group margin-bottom-0 margin-top-25">
                                    <input type="file" name="proImg[0][]" class="form-control p-img-file">
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-3">
                                <div class="form-check margin-top-25 def_img_radio">
                                    <label>
                                        <input type="radio" name="default_img[0][]" checked> <span class="label-text">Default Image</span>
                                        <input type="hidden" name="is_default_imf[0][]">
                                    </label>
                                </div>
                            </div>
                            <div class="col-lg-2 col-sm-3">
                                <button type="button" class="btn pull-right btn-primary add_product_img">
                                <span>
                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                </span>
                                </button>
    
                                <button type="button" class="btn pull-right btn-danger dlt_product_img">
                                <span>
                                    <i class="fa fa-minus-circle" aria-hidden="true"></i>
                                </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    
        <!--Weight Add Modal -->
        <div class="modal fade" id="addWeight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Weight</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" id="success_message" style="display:none"></div>
                        <div class="alert alert-danger" id="error_message" style="display: none"></div>
    
                        <form action="{{ url('settings/Weight/create') }}" id="weight_form" method="POST">
                        {{ csrf_field() }}
                        <!-- add category input-->
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="input-group margin-bottom-15">
                                        <label>Weight Name</label>
                                        <input type="text" class="form-control" value="" name="WeightName[]" id="WeightName">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary margin-top-22 add-weight-btn" type="button"><i class="fa fa-plus-circle"></i></button>
                                            <button class="btn btn-danger margin-top-22 dlt-weight-btn" type="button"><i class="fa fa-minus-circle"></i></button>
                                        </span>
                                    </div>
                                </div>
    
                                <div id="Weight-form-wrapper"></div>
    
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="save_button_weight">Save</button>
                    </div>
                </div>
            </div>
        </div>
    
        <!--Size Add Modal -->
        <div class="modal fade" id="addSize" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Size</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" id="success_message" style="display:none"></div>
                        <div class="alert alert-danger" id="error_message" style="display: none"></div>
    
                        <form id="size_form" action="{{ url('settings/size/create') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- add category input-->
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="input-group margin-bottom-15">
                                        <label>Size Name</label>
                                        <input type="text" class="form-control" value="" name="sizeName[]" id="sizeName">
                                        <span class="input-group-btn">
                                                <button class="btn btn-primary margin-top-22 add-size-btn" type="button"><i class="fa fa-plus-circle"></i></button>
                                                <button class="btn btn-danger margin-top-22 dlt-size-btn" type="button"><i class="fa fa-minus-circle"></i></button>
                                            </span>
                                    </div>
                                </div>
    
                                <div id="size-form-wrapper"></div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary margin-left-10" id="save_button_size">Save</button>
                    </div>
                </div>
            </div>
        </div>
    
        <!--Color Add Modal -->
        <div class="modal fade" id="addColor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Color</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" id="success_message" style="display:none"></div>
                        <div class="alert alert-danger" id="error_message" style="display: none"></div>
    
                        <form id="color_form" action="{{ url('settings/color/create') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- add category input-->
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="margin-bottom-15">
                                        <label>Color Name</label>
                                        <input type="text" class="form-control" value="" name="colorName" id="colorName">
                                    </div>
                                </div>
    
                                <div class="col-lg-6">
                                    <label>Color Code</label>
                                    <div id="cp2" class="input-group colorpicker-component margin-bottom-15">
                                        <input type="text" value="#000000" class="form-control" name="colorCode" id="colorCode21" />
                                        <span class="input-group-addon"><i></i></span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary" id="save_button_color">Save</button>
                    </div>
                </div>
            </div>
        </div>
    
        <!--Dimension Add Modal -->
        <div class="modal fade" id="addDimension" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Dimension</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" id="success_message" style="display:none"></div>
                        <div class="alert alert-danger" id="error_message" style="display: none"></div>
    
                        <form action="{{ url('settings/dimension/create') }}" id="form_dimension" method="POST">
                        {{ csrf_field() }}
                        <!-- add category input-->
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="input-group margin-bottom-15">
                                        <label>Dimension Name</label>
                                        <input type="text" class="form-control" value="" name="dimensionName[]" id="dimensionName">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary margin-top-22 add-dimension-btn" type="button"><i class="fa fa-plus-circle"></i></button>
                                            <button class="btn btn-danger margin-top-22 dlt-dimension-btn" type="button"><i class="fa fa-minus-circle"></i></button>
                                        </span>
                                    </div>
                                </div>
    
                                <div id="dimension-form-wrapper"></div>
    
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="save_dimension">Save</button>
                    </div>
                </div>
            </div>
        </div>
    
        <!--Add Tag Modal -->
        <div class="modal fade" id="addTag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                    </div>
                    <div class="modal-body">
                        <div class="alert alert-success" id="success_message" style="display:none"></div>
                        <div class="alert alert-danger" id="error_message" style="display: none"></div>
    
                        <form id="tag_form" action="{{ url('settings/tag/create') }}" method="POST">
                        {{ csrf_field() }}
                        <!-- add category input-->
                            <div class="row">
                                <div class="col-lg-8 col-lg-offset-2">
                                    <div class="input-group margin-bottom-15">
                                        <label>Tag Name</label>
                                        <input type="text" class="form-control" value="" name="tagName[]" id="tagName">
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary margin-top-22 add-tag-btn" type="button"><i class="fa fa-plus-circle"></i></button>
                                            <button class="btn btn-danger margin-top-22 dlt-tag-btn" type="button"><i class="fa fa-minus-circle"></i></button>
                                        </span>
                                    </div>
                                </div>
    
                                <div id="tag-form-wrapper"></div>
    
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-primary margin-left-10" id="save_button_tag">Save</button>
                    </div>
                </div>
            </div>
        </div>
    
    @endsection
    
    
    @section('js')
        <script src="{{ url('/back_assets/custom/sweetalert.js')}}"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/css/bootstrap-colorpicker.css" rel="stylesheet">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.1/js/bootstrap-colorpicker.min.js"></script>
        <script src="{{ url('/back_assets/plugins/holdon.min.js') }}"></script>
        <script type="text/javascript">
            var loadFile = function(event) {
                var reader = new FileReader();
                reader.onload = function(){
                    var output = document.getElementById('cat_image_preview');
                    output.src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            };
    
            var add_cat_loadFile = function(event) {
                var reader = new FileReader();
                reader.onload = function(){
                    var output = document.getElementById('add_cat_image_preview');
                    output.src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            };
    
    $(document).on('click', '#update_button', function(){
       
        category_update_form();
     });
     
     function category_update_form(){
        
        var formData = new FormData($('#category_update_form')[0]);
        

        var url = '{{ url('/settings/category/create') }}';
        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            success: function (data) {
                $("html, body").animate({ scrollTop: 0 }, "slow");
                if(data.status == 200){
                    $('#add_cat_modal').modal('hide');
                    $('#success_message').show();
                    $('#success_message').html(data.reason);
                    setTimeout(function(){
                        window.location.reload(true);
                        // window.location.href="{{ url('/product/add') }}";
                    }, 2000);

                }
                else{
                    $('#success_message').hide();
                    $('#error_message').show();
                    $('#error_message').html(data.reason);
                }
                
            },
            cache: false,
            contentType: false,
            processData: false
        });
    }

$(document).on('click', '#save_button_weight', function(){
             weight_form();
         });
         function weight_form(){
            
            var formData = new FormData($('#weight_form')[0]);
            
    
            var url = '{{ url('/settings/weight/create') }}';
            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                success: function (data) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    if(data.status == 200){
                        $('#addWeight').modal('hide');
                        $('#success_message').show();
                        $('#success_message').html(data.reason);
                        setTimeout(function(){
                            window.location.reload(true);
                            // window.location.href="{{ url('/product/add') }}";
                        }, 2000);

                    }
                    else{
                        $('#success_message').hide();
                        $('#error_message').show();
                        $('#error_message').html(data.reason);
                    }
                    
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        $(document).on('click', '#save_button_size', function(){
             size_form();
         });
         function size_form(){
            
            var formData = new FormData($('#size_form')[0]);
            
        //    console.log($('#form_color')[0]);
            var url = '{{ url('/settings/size/create') }}';
            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                success: function (data) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    if(data.status == 200){
                        $('#addSize').modal('hide');
                        $('#success_message').show();
                        $('#success_message').html(data.reason);
                        setTimeout(function(){
                            window.location.reload(true);
                            // window.location.href="{{ url('/product/add') }}";
                        }, 2000);

                    }
                    else{
                        $('#success_message').hide();
                        $('#error_message').show();
                        $('#error_message').html(data.reason);
                    }
                    
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        $(document).on('click', '#save_button_color', function(){
             color_form();
         });
         function color_form(){
            
            var formData = new FormData($('#color_form')[0]);
            
        //    console.log($('#form_color')[0]);
            var url = '{{ url('/settings/color/create') }}';
            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                success: function (data) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    if(data.status == 200){
                        $('#addColor').modal('hide');
                        $('#success_message').show();
                        $('#success_message').html(data.reason);
                        setTimeout(function(){
                            window.location.reload(true);
                            // window.location.href="{{ url('/product/add') }}";
                        }, 2000);

                    }
                    else{
                        $('#success_message').hide();
                        $('#error_message').show();
                        $('#error_message').html(data.reason);
                    }
                    
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        $(document).on('click', '#save_dimension', function(){
             dimension_form();
         });
         function dimension_form(){
            
            var formData = new FormData($('#form_dimension')[0]);
            var url = '{{ url('/settings/dimension/create') }}';
            $.ajax({
                type: "POST",
                url: url,
                data: formData,
                success: function (data) {
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    if(data.status == 200){
                        $('#addDimension').modal('hide');
                        $('#success_message').show();
                        $('#success_message').html(data.reason);
                        setTimeout(function(){
                            window.location.reload(true);
                            // window.location.href="{{ url('/product/add') }}";
                        }, 2000);

                    }
                    else{
                        $('#success_message').hide();
                        $('#error_message').show();
                        $('#error_message').html(data.reason);
                    }
                    
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }
        $(document).on('click', '#save_button_tag', function(){
             tag_form();
         });

         function tag_form(){
            
                var formData = new FormData($('#tag_form')[0]);
                var url = '{{ url('/settings/tag/create') }}';
                $.ajax({
                    type: "POST",
                    url: url,
                    data: formData,
                    success: function (data) {
                        $("html, body").animate({ scrollTop: 0 }, "slow");
                        if(data.status == 200){
                            $('#addTag').modal('hide');
                            $('#success_message').show();
                            $('#success_message').html(data.reason);
                            setTimeout(function(){
                                window.location.reload(true);   
                                // window.location.href="{{ url('/product/add') }}";
                            }, 2000);

                        }
                        else{
                            $('#success_message').hide();
                            $('#error_message').show();
                            $('#error_message').html(data.reason);
                        }
                        
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                });
            }

            $(document).on('submit', '#product_form', function(event){
                event.preventDefault();
                $('#product_status').val(1);
                submit_form();
            });
    
            $(document).on('click', '#save_button', function(){
                $('#product_status').val(1);
                submit_form();
            });
            $(document).on('click', '#save_draft_button', function(){
                $('#product_status').val(3);
                submit_form();
            });
    
            function submit_form(){
                var options = {
                theme:"sk-fading-circle",
                message:'Please wait while saving all data...',
                backgroundColor:"#1847B1",
                textColor:"white"
            };
            HoldOn.open(options);

                var title = $('#proTitle').val();
                var description = $('#proDesc').val();
                // var specification = $('#proSpec').val();
                var status = $('#proStatus').val();
                var nature = $('#proNature').val();
                var validate = '';
    
                if(title==''){
                    validate = validate+"Title is required</br>";
                }
                if(status==''){
                    validate = validate+"Status is required</br>";
                }
                if(nature==''){
                    validate = validate+"Nature is required</br>";
                }
                if(description==''){
                    validate = validate+"Description is required</br>";
                }
                // if(specification==''){
                //     validate = validate+"Specification is required</br>";
                // }
                isValid = 1;
                $("#product-info-panel input").not('#product-info-panel input[type=radio],#product-info-panel input[type=file]').each(function() {
                    var element = $(this);
                    if (element.val() == "") {
                        element.css('border','1px solid #f30000');
                        isValid = 0;
                    }
                });
                if(isValid==0){
                    validate = validate+"Fields in SKU section are required</br>";
                }
   
                if(validate==''){
                    var formData = new FormData($('#product_form')[0]);
                    // console.log(formData);
                    // exit();
                    var url = '{{ url('/product/update') }}';
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: formData,
                        async: false,
                        success: function (data) {
                            
                            $("html, body").animate({ scrollTop: 0 }, "slow");
                            if(data.status == 200){
                                $('#success_message').show();
                                $('#error_message').hide();
                                $('#success_message').html(data.reason);
                                setTimeout(function(){
                                    location.reload();
                                }, 2000);
    
                            }
                            else{
                                $('#success_message').hide();
                                $('#error_message').show();
                                $('#error_message').html(data.reason);
                            }
                        },
                        cache: false,
                        contentType: false,
                        processData: false
                    });
                }
                else{
                    $('#success_message').hide();
                    $('#error_message').show();
                    $('#error_message').html(validate);
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                }
            }
    
            //tag edit modal function
            $(document).on('click','.editProduct',function(){
                var text = $(this).parents('tr').children('.catTd').text();
                var id = $(this).attr('cus-data');
                $('#edit_modal').modal('show');
                $('#tagEname').val(text);
                $('#catId').val(id);
            })
    
            function tagSave(){
                var tagName = $('#tagName').val();
                if(tagName = ''){
                    alert(tagName);
                    return false;
                }
            }
    
            // Tag delete By ajax From Here-------------
            function tagDel(id){
                swal({
                            title: "Are you sure?",
                            text: "Your will not be able to recover this imaginary file!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "Yes, delete it!",
                            closeOnConfirm: false
                        },
                        function(){
                            var dataString = {
                                _token: "{{ csrf_token() }}",
                                id:id
                            };
                            $.ajax({
                                type: "POST",
                                url: "{{ url('/settings/category/delete') }}",
                                data: dataString,
                                dataType: "JSON",
                                cache : false,
                                beforeSend: function() {
    
                                },
                                success: function(data){ console.log(data);
                                    if(data.status == 200){
                                        $('#cat'+a).remove();
                                        swal("Deleted!", "Your data has been deleted.", "success");
                                    }
                                    if(data.status == 501){
                                        swal("Error!", "Delete Error.", "Error");
                                    }
                                } ,error: function(xhr, status, error) {
                                    alert(error);
                                },
                            });
    
    
                        });
            }
    
    
            $(document).on('click','.add_product_btn',function(){
    
                var children = $("#product-info-panel > div").length;
                var count = children+1;
    
                var html = '<div class="panel panel-default">';
                html += '<div class="panel-body" style="padding: 5px;">';
                html += '<div class="row no-margin">';
                html += '<div class="product_create_group col">';
                html += '<div class="input-group margin-bottom-15">';
                html += '<label> SKU<span class="mandatory"> *</span></label>';
                html += '<input type="text" class="form-control" value="" name="proSku[]" id="proSku">';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="margin-bottom-15">';
                html += '<label>Weight Unit<span class="mandatory"> *</span></label>';
                html += '<select class="form-control" name="weight_id[]" id="proSku">';
                html += '@foreach($weight_unit as $we)';
                html += '<option value="{{ $we->id }}">{{ $we->name }}</option>';
                html += '@endforeach';
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="input-group margin-bottom-15">';
                html += '<label>Weight<span class="mandatory"> *</span></label>';
                html += '<input type="text" class="form-control" value="" name="weight[]" id="proSku">';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="input-group margin-bottom-15">';
                html += '<label>Size<span class="mandatory"> *</span></label>';
                html += '<select class="form-control" name="proSize[]" id="proSize">';
                html += '@foreach($size as $sizes)';
                html += '<option value="{{ $sizes->id }}">{{ $sizes->name }}</option>';
                html += '@endforeach';
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="margin-bottom-15 p-color">';
                html += '<label>Color <span class="mandatory"> *</span></label>';
                html += '<select class="form-control color_select selectpicker" name="proColor[]" id="proColor">';
                html += '@foreach($color as $colors)';
                html += '<option value="{{ $colors->id }}" style="background: #5cb85c; color: #fff;">';
                html += '{{ $colors->display_name }}';
                html += '</option>';
                html += '@endforeach';
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="margin-bottom-15">';
                html += '<label>Dimention<span class="mandatory"> *</span></label>';
                html += '<select class="form-control" name="proDim[]" id="proDim">';
                html += '@foreach($dimesions as $dim)';
                html += '<option value="{{ $dim->id }}">{{ $dim->name }}</option>';
                html += '@endforeach';
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="margin-bottom-15 tag-select p-tag">';
                html += '<label class="pull-left">Tag<span class="mandatory"> *</span></label>';
                html += '<select class="form-control selectpicker" name="proTag['+children+'][]" id="proTag" multiple>';
                html += '@foreach($tags as $tag)';
                html += '<option value="{{ $tag->id }}">{{ $tag->name }}</option>';
                html += '@endforeach';
                html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="clearfix"></div>';
                html += '<div class="product_create_group col">';
                html += '<div class="input-group ">';
                html += '<label>Quantity<span class="mandatory"> *</span></label>';
                html += '<input type="text" class="form-control" value="" name="qty[]" id="qty">';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="input-group ">';
                html += '<label>Purchase Price<span class="mandatory"> *</span></label>';
                html += '<input type="text" class="form-control" value="" name="purchase_price[]" id="">';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="input-group ">';
                html += '<label>Salse Price<span class="mandatory"> *</span></label>';
                html += '<input type="text" class="form-control" value="" name="sales_price[]" id="">';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="">';
                html += '<label>Product Image<span class="mandatory"> *</span></label><br>';
                html += '<button type="button" class="btn btn-info p-img-btn btn-block" data-toggle="modal" data-target="#imgModal_'+ count +'">Upload</button>';
                html += '</div>';
    
                html += '<div class="modal fade" id="imgModal_'+ count +'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">';
                html += '<div class="modal-dialog modal-lg" role="document">';
                html += '<div class="modal-content">';
                html += '<div class="modal-header">';
                html += '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
                html += '<h4 class="modal-title" id="myModalLabel">Add Product Images</h4>';
                html += '</div>';
                html += '<div class="modal-body">';
                html += '<div class="row">';
                html += '<div class="col-lg-1 col-lg-offset-1 col-sm-2 no-padding">';
                html += '<div class="margin-bottom-15 p_image_preview_wrapper">';
                html += '<img class="p_image_preview" src="http://via.placeholder.com/1200x900">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-lg-5 col-sm-4">';
                html += '<div class="form-group margin-bottom-0 margin-top-25">';
                html += '<input type="file" name="proImg['+children+'][]" class="form-control p-img-file">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-lg-2 col-sm-3">';
                html += '<div class="form-check margin-top-25 def_img_check">';
                html += '<label>';
                html += '<input type="radio" name="default_img_'+children+'['+children+'][]" checked > <span class="label-text">Default Image</span>';
                html += '<input type="hidden" name="is_default_img['+children+'][]" value="1">';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-lg-2 col-sm-3">';
                html += '<button type="button" class="btn pull-right btn-primary add_product_img" data-id="'+children+'">';
                html += '<span>';
                html += '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                html += '</span>';
                html += '</button>';
                html += '<button type="button" disabled class="btn pull-right btn-danger dlt_product_img">';
                html += '<span>';
                html += '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
                html += '</span>';
                html += '</button>';
                html += '</div>';
                html += '</div>    ';
                html += '</div>';
                html += '<div class="modal-footer">';
                html += '<!--button type="button" class="btn btn-default" data-dismiss="modal">Close</button-->';
                html += '<button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
    
                html += '</div>';
                html += '<div class="product_create_group col">';
                html += '<div class="form-check margin-top-22 text-center p-def product_default">';
                html += '<label>';
                html += '<input type="radio" name="default_product[]"> <span class="label-text">Default</span>';
                html += '<input type="hidden" name="is_default[]" value="0"/>';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '<div class="product_create_group col margin-top-22 pull-right">';
                html += '<button type="button" class="btn pull-right btn-primary add_product_btn">';
                html += '<span>';
                html += '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                html += '</span>';
                html += '</button>';
                html += '<button type="button" class="btn pull-right btn-danger dlt_product_btn">';
                html += '<span>';
                html += '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
                html += '</span>';
                html += '</button>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
    
                $(this).parents('#product-info-panel').append(html);
    
                $('.selectpicker').selectpicker();
            });
    
            $(document).on('click','.dlt_product_btn',function(){
                $(this).parents('.panel').eq(0).remove();
            });
    
            $(document).ready(function() {
                $('.summernote').summernote({
                    height: 90
                });
            });
    
    
            //Image upload modal add
            $(document).on('click','.add_product_img',function(){
                var row_number = $(this).attr('data-id');
                var html = '<div class="row">';
                html += '<div class="col-lg-1 col-lg-offset-1 col-sm-2 no-padding">';
                html += '<div class="margin-bottom-15 p_image_preview_wrapper">';
                html += '<img class="p_image_preview" src="http://via.placeholder.com/1200x900">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-lg-5 col-sm-4">';
                html += '<div class="form-group margin-bottom-0 margin-top-25">';
                html += '<input type="file" name="proImg['+row_number+'][]" class="form-control p-img-file">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-lg-2 col-sm-3">';
                html += '<div class="form-check margin-top-25 def_img_check">';
                html += '<label>';
                html += '<input type="radio" name="default_img['+row_number+'][]"> <span class="label-text">Default Image</span>';
                html += '<input type="hidden" name="is_default_img['+row_number+'][]" value="0">';
                html += '</label>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-lg-2 col-sm-3">';
                html += '<button type="button" class="btn pull-right btn-primary add_product_img"data-id="'+row_number+'">';
                html += '<span>';
                html += '<i class="fa fa-plus-circle" aria-hidden="true"></i>';
                html += '</span>';
                html += '</button>';
                html += '<button type="button" class="btn pull-right btn-danger dlt_product_img">';
                html += '<span>';
                html += '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
                html += '</span>';
                html += '</button>';
                html += '</div>';
                html += '</div>';
    
                $(this).parents('.modal-body').append(html);
    
            });
    
            $(document).on('click','.dlt_product_img',function(){
                $(this).parents('.row').eq(0).remove();
            });
    
            $(document).on('change','.p-img-file',function(event){
                var tmppath = URL.createObjectURL(event.target.files[0]);
                $(this).parents('.row').eq(0).find('.p_image_preview').attr('src',URL.createObjectURL(event.target.files[0]));
            });
    
            //color picker
            $(document).ready(function() {
                $('#colorCode21, #cp2').colorpicker();
            });
    
            //Add specific type
            $('#add_extra_type_btn').click(function(){
                var type_name = $('#add_extra_type').val();
    
                if(type_name == 'Weight'){
                    $('#addWeight').modal('show');
                }
                else if(type_name == 'Size'){
                    $('#addSize').modal('show');
                }
                else if(type_name == 'Color'){
                    $('#addColor').modal('show');
                }
                else if(type_name == 'Dimension'){
                    $('#addDimension').modal('show');
                }
                else if(type_name == 'Tag'){
                    $('#addTag').modal('show');
                }
            });
    
            //Weight add
            $(document).on('click','.add-weight-btn',function(){
                var html ='<div class="col-lg-8 col-lg-offset-2 size-form-item">';
                html+='<div class="input-group margin-bottom-15">';
                html+='<input type="text" class="form-control" value="" name="weightName[]" id="">';
                html+='<span class="input-group-btn">';
                html+='<button class="btn btn-primary add-weight-btn" type="button"><i class="fa fa-plus-circle"></i></button>';
                html+='<button class="btn btn-danger dlt-weight-btn" type="button"><i class="fa fa-minus-circle"></i></button>';
                html+='</span>';
                html+='</div>';
                html+='</div>';
    
                $('#Weight-form-wrapper').append(html);
            });
    
            $(document).on('click','.dlt-weight-btn',function(){
                $(this).parents('.size-form-item').remove();
            });
    
            //Size add
            $(document).on('click','.add-size-btn',function(){
                var html ='<div class="col-lg-8 col-lg-offset-2 size-form-item">';
                html+='<div class="input-group margin-bottom-15">';
                html+='<input type="text" class="form-control" value="" name="sizeName[]" id="">';
                html+='<span class="input-group-btn">';
                html+='<button class="btn btn-primary add-size-btn" type="button"><i class="fa fa-plus-circle"></i></button>';
                html+='<button class="btn btn-danger dlt-size-btn" type="button"><i class="fa fa-minus-circle"></i></button>';
                html+='</span>';
                html+='</div>';
                html+='</div>';
    
                $('#size-form-wrapper').append(html);
            });
    
            $(document).on('click','.dlt-size-btn',function(){
                $(this).parents('.size-form-item').remove();
            });
    
            //Dimension add
            $(document).on('click','.add-dimension-btn',function(){
                var html ='<div class="col-lg-8 col-lg-offset-2 size-form-item">';
                html+='<div class="input-group margin-bottom-15">';
                html+='<input type="text" class="form-control" value="" name="dimensionName[]" id="">';
                html+='<span class="input-group-btn">';
                html+='<button class="btn btn-primary add-dimension-btn" type="button"><i class="fa fa-plus-circle"></i></button>';
                html+='<button class="btn btn-danger dlt-dimension-btn" type="button"><i class="fa fa-minus-circle"></i></button>';
                html+='</span>';
                html+='</div>';
                html+='</div>';
    
                $('#dimension-form-wrapper').append(html);
            });
    
            $(document).on('click','.dlt-size-btn',function(){
                $(this).parents('.size-form-item').remove();
            });
    
            //Tag add
            $(document).on('click','.add-tag-btn',function(){
                var html ='<div class="col-lg-8 col-lg-offset-2 tag-form-item">';
                html+='<div class="input-group margin-bottom-15">';
                html+='<input type="text" class="form-control" value="" name="tagName[]" id="">';
                html+='<span class="input-group-btn">';
                html+='<button class="btn btn-primary add-tag-btn" type="button"><i class="fa fa-plus-circle"></i></button>';
                html+='<button class="btn btn-danger dlt-tag-btn" type="button"><i class="fa fa-minus-circle"></i></button>';
                html+='</span>';
                html+='</div>';
                html+='</div>';
    
                $('#tag-form-wrapper').append(html);
            });
    
            $(document).on('click','.dlt-tag-btn',function(){
                $(this).parents('.tag-form-item').remove();
            });
    
            $(document).on('click','.product_default',function(){
                $('.product_default').parents('#product-info-panel').find('input[name="is_default[]"]').val(0);
                $(this).find('input[name="is_default[]"]').val(1);
            });
    
            $(document).on('click','.def_img_check',function(){
                $(this).parents('.modal-body').eq(0).find('.def_img_check').children('input[type="hidden"]').val(0);
                $(this).find('input[type="hidden"]').val(1);
            });
        </script>
    
    @endsection


                        

                                    